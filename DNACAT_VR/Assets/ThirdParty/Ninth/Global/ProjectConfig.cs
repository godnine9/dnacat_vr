using Ninth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth
{
	[ConfigPath("Config/DataSetConfig")]
	public class ProjectConfig : Config<ProjectConfig>
	{
		public string Version; 
	}
}

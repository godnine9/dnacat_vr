﻿using System.Collections;

namespace Ninth.GameFlowSystem
{
	public class UIConnecting : UILoading
	{
		public override float Progress
		{
			get;
			set;
		}

		public override IEnumerator Open()
		{
			gameObject.SetActive(true);
			yield break;
		}

		public override IEnumerator Close()
		{
			gameObject.SetActive(false);
			yield break;
		}
	}
}

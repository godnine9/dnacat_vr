﻿using System.Collections;
using UnityEngine;

namespace Ninth.GameFlowSystem
{
	public class UILoadingExample : UILoading
	{
		[SerializeField]
		private CanvasGroup canvasGroup = null;

		[SerializeField, Range(0.01f, 1f)]
		private float fadeInSpeed = 0.02f;

		[SerializeField, Range(0.01f, 1f)]
		private float fadeOutSpeed = 0.02f;

		public override float Progress
		{
			get => imgProgress.fillAmount;
			set
			{
				imgProgress.fillAmount = value;
				txtProgress.text = $"{value * 100f:#0}%";
			}
		}

		public override IEnumerator Open()
		{
			canvasGroup.alpha = 0f;
			gameObject.SetActive(true);
			while (canvasGroup.alpha < 1f)
			{
				canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 1f, fadeInSpeed);
				yield return null;
			}
		}

		public override IEnumerator Close()
		{
			canvasGroup.alpha = 1f;
			while (canvasGroup.alpha > 0f)
			{
				canvasGroup.alpha = Mathf.MoveTowards(canvasGroup.alpha, 0f, fadeOutSpeed);
				yield return null;
			}
			gameObject.SetActive(false);
		}
	}
}

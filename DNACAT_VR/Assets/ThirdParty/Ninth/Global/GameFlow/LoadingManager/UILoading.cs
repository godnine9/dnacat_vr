﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Ninth.GameFlowSystem
{
	public abstract class UILoading : MonoBehaviour
	{
		[SerializeField]
		protected Image imgProgress = null;
		[SerializeField]
		protected Text txtProgress = null;

		public abstract float Progress
		{
			get;
			set;
		}

		public virtual IEnumerator Open()
		{
			gameObject.SetActive(true);
			yield return null;
		}

		public virtual IEnumerator Close()
		{
			gameObject.SetActive(false);
			yield return null;
		}
	}
}
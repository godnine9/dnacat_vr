﻿using System;
using System.Collections;
using UnityEngine;

namespace Ninth.GameFlowSystem
{
	[Serializable]
	public class LoadingManager
	{
		[SerializeField]
		private UILoading[] ui = null;

		private UILoading current = null;

		public bool IsLoading => current != null;

		public float Progress
		{
			get => current.Progress;
			set => current.Progress = value;
		}

		public bool IsUIValid(string name)
		{
			return Array.Exists(ui, Exist);

			bool Exist(UILoading loadingUI)
			{
				return loadingUI.name.Equals(name);
			}
		}

		public IEnumerator OpenUI(string name)
		{
			current = Array.Find(ui, loadingUI => loadingUI.name == name);

			if (current)
			{
				Progress = 0f;
				yield return current.Open();
			}
			else
			{
				yield break;
			}
		}

		public IEnumerator CloseUI()
		{
			if (IsLoading)
			{
				yield return current.Close();
				current = null;
			}
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;
using Ninth.GamePlay.DNACAT;

namespace Ninth.GameFlowSystem
{
	public class PhotonEnterRoom : GameFlowState
	{
		[SerializeField]
		private string loadingUI = string.Empty;
		[SerializeField]
		private string sceneName = string.Empty;
		[SerializeField]
		private float delayLoadingOver = 0.2f;
		[SerializeField]
		private string[] additiveSceneNames = null;

		private Coroutine loadingHandle;

		public override void OnEnter(Animator animator)
		{
			if (loadingHandle != null)
			{
				Global.StopCoroutine(loadingHandle);
			}
			loadingHandle = Global.StartCoroutine(UntilLoadLevelFinished());
		}

		private IEnumerator UntilLoadLevelFinished()
		{
			yield return LocalizationDataSet.LoadAsync("Data/LocalizationDataSet");
			yield return WeaponDataSet.LoadAsync("Data/WeaponDataSet");
			yield return PlayerLevelDataSet.LoadAsync("Data/PlayerLevelDataSet");
			yield return EnemyDataSet.LoadAsync("Data/EnemyDataSet");
			yield return ItemDataSet.LoadAsync("Data/ItemDataSet");

			PlayerLevelPreset.Initialize(PlayerLevelDataSet.DataContainer);
			yield return LoadingManager.OpenUI(loadingUI);
			PhotonNetwork.AutomaticallySyncScene = true;

			PhotonNetwork.JoinOrCreateRoom("Main", new RoomOptions() { MaxPlayers = 0 }, TypedLobby.Default);
			yield return new WaitUntil(() => Global.Photon.JoinedRoom);

			if (PhotonNetwork.IsMasterClient)
			{
				PhotonNetwork.LoadLevel(sceneName);
				while (LoadingManager.Progress < 1f)
				{
					LoadingManager.Progress = PhotonNetwork.LevelLoadingProgress;
					yield return null;
				}
			}
			LoadingManager.Progress = 1f;

			yield return LoadAdditiveScenes();
			yield return new WaitForSeconds(delayLoadingOver);
			yield return LoadingManager.CloseUI();
		}

		private IEnumerator LoadAdditiveScenes()
		{
			for (int i = 0; i < additiveSceneNames.Length; i++)
			{
				string sceneName = additiveSceneNames[i];
				yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
			}
		}
	}
}

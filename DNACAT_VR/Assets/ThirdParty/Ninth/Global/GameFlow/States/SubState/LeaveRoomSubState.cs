using System.Collections;
using UnityEngine;
using Photon.Pun;

namespace Ninth.GameFlowSystem
{
	public class LeaveRoomSubState : SubState
	{
		public override IEnumerator OnPreProcess()
		{
			//PhotonNetwork.AutomaticallySyncScene = false;
			if (!Global.Photon.JoinedRoom)
			{
				yield break;
			}

			if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount > 1)
			{
				PhotonNetwork.SetMasterClient(PhotonNetwork.PlayerList[0]);
			}

			PhotonNetwork.LeaveRoom();
			yield return new WaitUntil(() => !Global.Photon.JoinedRoom);
		}
	}
}

﻿using Ninth.GamePlay.DNACAT;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ninth.GameFlowSystem
{
	public class LoadSceneState : GameFlowState
	{
		[SerializeField]
		private string loadingUI = string.Empty;
		[SerializeField]
		private string sceneName = string.Empty;
		[SerializeField]
		private float delayLoadingOver = 0.5f;
		[SerializeField]
		private string[] additiveSceneNames = null;
		[SerializeField]
		private SubState[] subStates = null;

		public override void OnEnter(Animator animator)
		{
			IEnumerator process = LoadingManager.IsUIValid(loadingUI) ? LoadWithLoadingUI(loadingUI) : Load();
			Global.StartCoroutine(process);
		}

		private IEnumerator Load()
		{
			yield return SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
			yield return LoadAdditiveScenes();
		}

		private IEnumerator LoadWithLoadingUI(string loadingUI)
		{
			yield return LoadingManager.OpenUI(loadingUI);

			for (int i = 0; i < subStates.Length; i++)
			{
				yield return subStates[i].OnPreProcess();
			}

			var mainSceneOp = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
			mainSceneOp.allowSceneActivation = false;
			SceneManager.sceneLoaded += OnSceneUnloaded;
			
			while (LoadingManager.Progress < 0.9f)
			{
				//LoadingManager.Progress = Mathf.MoveTowards(LoadingManager.Progress, mainSceneOp.progress, Time.deltaTime);
				LoadingManager.Progress = mainSceneOp.progress;
				yield return null;
			}

			mainSceneOp.allowSceneActivation = true;

			for (int i = 0; i < subStates.Length; i++)
			{
				yield return subStates[i].OnPostProcess();
			}

			yield return LoadAdditiveScenes();

			while (LoadingManager.Progress < 1f)
			{
				LoadingManager.Progress = Mathf.MoveTowards(LoadingManager.Progress, 1f, Time.deltaTime);
				yield return null;
			}

			yield return new WaitForSeconds(delayLoadingOver);
			yield return LoadingManager.CloseUI();

			void OnSceneUnloaded(Scene scene, LoadSceneMode mode)
			{
				mainSceneOp.allowSceneActivation = true;
				SceneManager.sceneLoaded -= OnSceneUnloaded;
				UIHelper.CursorState = true;
				AudioManager.StopAllMusic();
			}
		}

		private IEnumerator LoadAdditiveScenes()
		{
			var ops = new List<IEnumerator>();
			for (int i = 0; i < additiveSceneNames.Length; i++)
			{
				string sceneName = additiveSceneNames[i];
				ops.Add(HandleLoadScene(sceneName, LoadSceneMode.Additive));
			}
			yield return Global.ParallelTasks(ops);

			IEnumerator HandleLoadScene(string sceneName, LoadSceneMode mode)
			{
				yield return SceneManager.LoadSceneAsync(sceneName, mode);
			}
		}
	}
}
﻿using System.Collections;
using UnityEngine;
using Photon.Pun;

namespace Ninth.GameFlowSystem
{
	public class PhotonConnection : GameFlowState
	{
		private const string uiConnectingName = "ConnectingUI";

		public override void OnEnter(Animator animator)
		{			
			Global.StartCoroutine(ConnectToMaster());
			LoadingManager.OpenUI(uiConnectingName);
		}

		public override void OnExit(Animator animator)
		{
			LoadingManager.CloseUI();
		}

		private IEnumerator ConnectToMaster()
		{
			PhotonNetwork.ConnectUsingSettings();
			yield return new WaitUntil(() => Global.Photon.ConnectedToMaster);
			Global.GameFlow.Finish();
		}
	}
}

﻿using System;
using UnityEngine;

namespace Ninth.GameFlowSystem
{
	[Serializable]
	public class GameFlow
	{
		public const string TriggerKeyFinish = "Finish";

		[SerializeField]
		private Animator animator = null;

		public string CurrentBranch { get; private set; }

		public void Finish() => animator.SetTrigger(TriggerKeyFinish);

		public void BranchTo(string key, bool value)
		{
			if (!string.IsNullOrEmpty(key))
			{
				animator.SetBool(key, value);
				CurrentBranch = key;
			}
		}

		public void BranchBack()
		{
			if (!string.IsNullOrEmpty(CurrentBranch))
			{
				animator.SetBool(CurrentBranch, false);
				CurrentBranch = string.Empty;
			}
		}
	}
}
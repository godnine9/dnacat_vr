﻿using UnityEngine;
using UnityEngine.Animations;

namespace Ninth.GameFlowSystem
{
	public abstract class GameFlowState : StateMachineBehaviour
	{
		protected Global Global => Global.Instance;

		protected LoadingManager LoadingManager => Global.LoadingManager;

		public virtual void OnEnter(Animator animator)
		{
		}

		public virtual void OnUpdate(Animator animator)
		{
		}

		public virtual void OnExit(Animator animator)
		{
		}

		public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			OnEnter(animator);
		}

		public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			OnExit(animator);
		}

		public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			OnUpdate(animator);
		}

		#region Useless
		public sealed override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
		}

		public sealed override void OnStateMachineEnter(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateMachineExit(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
		}

		public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		public sealed override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		public sealed override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}
		#endregion
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth
{
	using GameFlowSystem;
	using Ninth.GamePlay.DNACAT;
	using UnityEngine.EventSystems;

	public class Global : Singleton<Global>
	{
		[SerializeField]
		private GameFlow gameFlow = null;

		[SerializeField]
		private LoadingManager loadingManager = null;

		[SerializeField]
		private PhotonMessageHandler punMessageHandler = null;

		public GameFlow GameFlow => gameFlow;

		public LoadingManager LoadingManager => loadingManager;

		public PhotonMessageHandler Photon => punMessageHandler;

		public GameObject CurrentSelectedUI => EventSystem.current.currentSelectedGameObject;

		public IEnumerator SequentialTasks(IList<IEnumerator> enumerators)
		{
			for (int i = 0; i < enumerators.Count; ++i)
			{
				yield return enumerators[i];
			}
		}

		/// <summary>
		/// TODO:Performance
		/// </summary>
		/// <param name="enumerators"></param>
		/// <returns></returns>
		public IEnumerator ParallelTasks(IList<IEnumerator> enumerators)
		{
			var coroutines = new Coroutine[enumerators.Count];
			for (int i = 0; i < enumerators.Count; i++)
			{
				coroutines[i] = StartCoroutine(enumerators[i]);
			}

			for (int i = 0; i < coroutines.Length; i++)
			{
				yield return coroutines[i];
			}
		}

		public IEnumerator<T> ParallelTasks<T>(IList<IEnumerator<T>> enumerators)
		{
			while (true)
			{
				bool isDone = false;
				T current = default;
				for (int i = 0; i < enumerators.Count; i++)
				{
					var enumerator = enumerators[i];
					if (enumerator.MoveNext())
					{
						current = enumerator.Current;
						isDone ^= false;
					}
				}
				
				if (isDone)
				{
					yield break;
				}
				yield return current;
			}
		}
	}
}
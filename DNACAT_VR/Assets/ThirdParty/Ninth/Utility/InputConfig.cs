using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	[ConfigPath("Config/InputConfig")]
	public class InputConfig : Config<InputConfig>
	{
		public InputSource Source;
	}
}

﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class SceneSingleton<T> : MonoBehaviour where T : SceneSingleton<T>
	{
		public bool IsInitialized => _instance != null;

		private static readonly object _lock = new object();

		private static T _instance;

		public static T Instance
		{
			get
			{
				lock (_lock)
				{
					Initialize();
					return _instance;
				}
			}
		}

		private static void Initialize()
		{
			if (_instance == null)
			{
				var objs = FindObjectsOfType<T>();
				if (objs.Length > 0)
				{
					_instance = objs[0];
				}

				if (objs.Length > 1)
				{
					for (int i = 1; i < objs.Length; i++)
					{
						Destroy(objs[i]);
					}
				}

				if (_instance == null)
				{
					var instanceObj = new GameObject($"[SceneSingleton]{nameof(T)}");
					_instance = instanceObj.AddComponent<T>();
				}

				_instance.OnInitialized();
			}
		}

		protected virtual void Awake()
		{
			if (IsInitialized)
			{
				Destroy(this);
			}
			else
			{
				Initialize();
			}
		}

		protected virtual void OnInitialized()
		{
		}
	}
}
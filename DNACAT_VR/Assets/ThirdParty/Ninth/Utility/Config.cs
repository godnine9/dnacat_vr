﻿using System.Reflection;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Ninth
{
	public class Config<T> : ScriptableObject where T : Config<T>
	{
		private static T instance = null;

		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					string path = GetSavePath();
					instance = Resources.Load<T>(GetSavePath());
					if (instance == null)
					{
#if UNITY_EDITOR
						string createPath = $"Assets/Resources/{path}.asset";
						instance = CreateInstance<T>();
						AssetDatabase.CreateAsset(instance, createPath);
						AssetDatabase.Refresh();
						AssetDatabase.SaveAssets();
#else
						Debug.LogError($"Config not found: {path}");
#endif
					}
				}
				return instance;
			}
		}

		public static string GetSavePath()
		{
			var attribite = typeof(T).GetCustomAttribute<ConfigPathAttribute>();
			if (attribite != null)
			{
				return attribite.Path;
			}
			return nameof(T);
		}
	}
}
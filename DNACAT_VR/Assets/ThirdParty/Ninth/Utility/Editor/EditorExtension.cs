﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Ninth
{
	public class EditorExtension
	{
		[MenuItem("Ninth/QuickStart %`")]
		public static void QuickStart()
		{
			if (EditorApplication.isPlaying)
			{
				Debug.LogError("QuickStart");
				return;
			}

			var currentEditScene = SceneManager.GetActiveScene();

			if (currentEditScene.isDirty)
			{
				if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
				{
					if (!string.IsNullOrEmpty(currentEditScene.path))
					{
						EditorSceneManager.SaveScene(currentEditScene);
					}
				}
				else
				{
					return;
				}
			}

			if (EditorBuildSettings.scenes.Length > 0)
			{
				EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
				EditorApplication.EnterPlaymode();
			}
			else
			{
				Debug.LogError("No scenes found in BuildSettings.");
			}
		}
	}
}

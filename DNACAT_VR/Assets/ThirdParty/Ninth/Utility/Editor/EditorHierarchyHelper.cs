﻿using UnityEngine;
using UnityEditor;

namespace Ninth
{
	public class EditorHierarchyHelper
	{
		/// <summary>
		/// Get current selection gameObject which has the certain component.
		/// If not, try to find a gameObject with the component in hierarchy.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="parent"></param>
		/// <returns></returns>
		public static bool TryGetCurrentSelectionAsParent<T>(out Transform parent) where T : Component
		{
			parent = null;
			if (!Selection.activeGameObject)
			{
				var canvasObj = Object.FindObjectOfType(typeof(T));
				if (canvasObj)
				{
					parent = (canvasObj as Canvas).transform;
				}
			}
			else
			{
				if (Selection.activeGameObject.TryGetComponent<T>(out var canvas))
				{
					parent = canvas.transform;
				}

				if (!parent)
				{
					if (Selection.activeGameObject.transform.root.TryGetComponent<T>(out _))
					{
						parent = Selection.activeGameObject.transform;
					}
					if (!parent)
					{
						parent = Selection.activeGameObject.transform;
					}
				}
			}

			return parent;
		}
	}
}

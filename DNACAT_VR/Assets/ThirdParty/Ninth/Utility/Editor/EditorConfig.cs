﻿using System.Reflection;
using System.IO;
using UnityEngine;
using UnityEditor;

namespace Ninth
{
	/// <summary>
	/// ScriptableObject access for UnityEditor only.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class EditorConfig<T> : ScriptableObject where T : EditorConfig<T>
	{
		private static T instance = null;

		public static T Instance
		{
			get
			{
				if (instance == null)
				{
					instance = AssetDatabase.LoadAssetAtPath<T>(GetSavePath());
					if (instance == null)
					{
						instance = CreateConfigInstance();
					}
				}
				return instance;
			}
		}

		public static string GetSavePath()
		{
			string defaultPath = $"Assets/Editor/{nameof(T)}.asset";
			var attribite = typeof(T).GetCustomAttribute<ConfigPathAttribute>();
			if (attribite == null)
			{
				Debug.LogWarning($"Config class<{nameof(T)}> does not have 'ConfigSavePathAttribute'.");
				return defaultPath;
			}

			if (!Directory.GetParent(attribite.Path).Name.Equals("Editor"))
			{
				Debug.LogWarning($"Path from 'ConfigSavePathAttribute' is not under an Editor folder.\nConfig: {nameof(T)}\nPath: {attribite.Path}");
				return defaultPath;
			}

			return attribite.Path;
		}

		public static T CreateConfigInstance()
		{
			var instance = CreateInstance<T>();
			AssetDatabase.CreateAsset(instance, GetSavePath());
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
			EditorGUIUtility.PingObject(instance);
			return instance;
		}

		public static bool IsInstanceAlive => instance != null;
	}
}

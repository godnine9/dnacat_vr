﻿using System;

namespace Ninth
{
	/// <summary>
	/// From 'Resources' folder.
	/// </summary>
	public class ConfigPathAttribute : Attribute
	{
		public string Path = string.Empty;

		public ConfigPathAttribute(string path)
		{
			Path = path;
		}
	}
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ninth
{
	public abstract class DataSet<T> : DataSet where T : BaseData
	{
		[SerializeField]
		protected List<T> data = null;

		public static IReadOnlyDictionary<int, T> DataContainer = null;

		public static DataSet<T> Instance
		{ 
			get;
			private set;
		}

		public IReadOnlyList<T> Data
		{
			get => data;
		}

		/// <summary>
		/// Load data where path is under the 'Resources' folder.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public static IEnumerator LoadAsync(string path)
		{
			var loadHandle = Resources.LoadAsync<DataSet<T>>(path);
			while (!loadHandle.isDone)
			{
				yield return null;
			}
			if (loadHandle.asset != null)
			{
				OnLoadCompleted(loadHandle);
			}
		}

		public static ResourceRequest GetLoadAsyncRequest(string path)
		{
			var request = Resources.LoadAsync<DataSet<T>>(path);
			request.completed += OnLoadCompleted;
			return request;
		}

		private static void OnLoadCompleted(AsyncOperation asyncOperation)
		{
			if (asyncOperation is ResourceRequest request)
			{
				Instance = request.asset as DataSet<T>;
				DataContainer = Instance.data.ToDictionary(data => data.ID);
				Debug.Log($"Load {Instance.GetType().Name}");
			}
			else
			{
				Debug.LogError($"Failed to load {Instance.GetType().Name}");
			}
		}

		public IList<int> GetAllID()
		{
			return DataContainer.Keys.ToList();
		}

		public IList<T> GetAllData()
		{
			return DataContainer.Values.ToList();
		}

		public IEnumerable<T> FindAll(Func<T, bool> predicate)
		{
			return DataContainer.Values.Where(predicate).ToArray();
		}

		public bool TryGetFirst(Func<T, bool> predicate, out T data)
		{
			data = DataContainer.Values.FirstOrDefault(predicate);
			return data != null;
		}

		public bool TryGetData(int id, out T data)
		{
			return DataContainer.TryGetValue(id, out data);
		}
	}

	public abstract class DataSet : ScriptableObject
	{
		private void OnEnable()
		{
			hideFlags = HideFlags.NotEditable;
		}
	}
}

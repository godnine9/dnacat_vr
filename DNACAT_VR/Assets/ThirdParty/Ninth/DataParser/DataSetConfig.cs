﻿namespace Ninth
{
    [ConfigPath("Config/DataSetConfig")]
    public class DataSetConfig : Config<DataSetConfig>
    {
        public string DataSetSavePath = "Data";
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Ninth
{
	public abstract class EditorDataSet<T> where T : BaseData
	{
		private const string editorDataSetSavePathFormat = "Assets/ThirdParty/Ninth/DataParser/Resources/Data/{0}.asset";

		public static IReadOnlyDictionary<int, T> DataContainer = null;

		public static int MinID
		{
			get;
			private set;
		}

		public static int MaxID
		{
			get;
			private set;
		}

		public static DataSet<T> Instance
		{
			get;
			private set;
		}

		protected static void Load(string name)
		{
			string path = string.Format(editorDataSetSavePathFormat, name);
			Instance = AssetDatabase.LoadAssetAtPath<DataSet<T>>(path);

			if (!Instance)
			{
				Debug.LogError($"EditorDataSet not found: {path}");
				return;
			}

			DataContainer = Instance.Data.ToDictionary(data => data.ID);
			MinID = DataContainer.Min(preset => preset.Key);
			MaxID = DataContainer.Max(preset => preset.Key);
		}

		public bool TryGetData(int key, out T data)
		{
			return DataContainer.TryGetValue(key, out data);
		}

		public IList<int> GetAllID()
		{
			return DataContainer.Keys.ToList();
		}

		public IList<T> GetAllData()
		{
			return DataContainer.Values.ToList();
		}
	}
}

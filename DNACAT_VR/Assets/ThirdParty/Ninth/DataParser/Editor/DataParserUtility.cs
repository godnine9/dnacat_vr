﻿using System;
using System.IO;
using System.CodeDom;
using System.Text.RegularExpressions;

namespace Ninth
{
	public class DataParserUtility
	{
		public static bool MakeDataCode(string dataName, (string, string)[] fields, string path)
		{
			var compiler = new CodeCompileUnit();

			// Namespace
			var @namespace = new CodeNamespace(DataParserConfig.Instance.Namespace);
			compiler.Namespaces.Add(@namespace);

			// Class
			if (string.IsNullOrEmpty(dataName))
			{
				return false;
			}

			var @class = new CodeTypeDeclaration(dataName);
			@class.BaseTypes.Add(typeof(BaseData));
			@class.CustomAttributes.Add(new CodeAttributeDeclaration(new CodeTypeReference(typeof(SerializableAttribute))));

			var constructor = new CodeConstructor
			{
				Attributes = MemberAttributes.Public,
			};

			// Fields
			if (fields != null)
			{
				for (int i = 1; i < fields.Length; ++i)
				{
					string fieldType = fields[i].Item1;
					string fieldName = fields[i].Item2;

					var member = new CodeMemberField(fieldType, fieldName)
					{
						Attributes = MemberAttributes.Public,
					};
					@class.Members.Add(member);
				}
			}
			@class.Members.Add(constructor);
			@namespace.Types.Add(@class);
			using (var writer = new StreamWriter(path))
			{
				DataParserConfig.DOMProvider.GenerateCodeFromCompileUnit(compiler, writer, DataParserConfig.DOMOptions);
			}

			return true;
		}

		public static bool MakeDataSetCode(string dataSetName, string dataName, string path)
		{
			var compiler = new CodeCompileUnit();

			// Namespace
			var @namespace = new CodeNamespace(DataParserConfig.Instance.Namespace);
			compiler.Namespaces.Add(@namespace);

			// Class
			if (string.IsNullOrEmpty(dataSetName))
			{
				return false;
			}

			var @class = new CodeTypeDeclaration(dataSetName);
			@class.BaseTypes.Add(new CodeTypeReference($"{nameof(DataSet)}<{dataName}>"));
			@namespace.Types.Add(@class);
			using (var writer = new StreamWriter(path))
			{
				DataParserConfig.DOMProvider.GenerateCodeFromCompileUnit(compiler, writer, DataParserConfig.DOMOptions);
			}

			return true;
		}

		public static bool TryGetEndUnsignedNumber(string text, out int number)
		{
			if (!int.TryParse(Regex.Match(text, @"\d+$").Value, out number))
			{
				return false;
			}

			if (number < 0)
			{
				number *= -1;
			}
			return true;
		}
	}
}

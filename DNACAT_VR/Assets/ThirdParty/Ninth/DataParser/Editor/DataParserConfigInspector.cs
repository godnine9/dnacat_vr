﻿using UnityEditor;
using UnityEngine;

namespace Ninth
{
	[CustomEditor(typeof(DataParserConfig))]
	public class DataParserConfigInspector : Editor
	{
		private SerializedProperty excelPathSP = null;
		private SerializedProperty codeSavePathSP = null;
		private SerializedProperty dataSavePathSP = null;
		private SerializedProperty namespaceSP = null;
		private SerializedProperty dataSuffixSP = null;
		private SerializedProperty dataSetSuffixSP = null;
		private SerializedProperty keyFieldNameSP = null;

		private void OnEnable()
		{
			excelPathSP = serializedObject.FindProperty("ExcelPath");
			codeSavePathSP = serializedObject.FindProperty("CodeSavePath");
			dataSavePathSP = serializedObject.FindProperty("DataSavePath");
			namespaceSP = serializedObject.FindProperty("Namespace");
			dataSuffixSP = serializedObject.FindProperty("DataSuffix");
			dataSetSuffixSP = serializedObject.FindProperty("DataSetSuffix");
			keyFieldNameSP = serializedObject.FindProperty("KeyFieldName");
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			EditorGUILayout.PropertyField(excelPathSP);
			serializedObject.ApplyModifiedProperties();

			GUI.enabled = false;
			EditorGUILayout.PropertyField(codeSavePathSP);
			EditorGUILayout.PropertyField(dataSavePathSP);
			EditorGUILayout.PropertyField(namespaceSP);
			EditorGUILayout.PropertyField(dataSuffixSP);
			EditorGUILayout.PropertyField(dataSetSuffixSP);
			EditorGUILayout.PropertyField(keyFieldNameSP);
			GUI.enabled = true;
		}
	}
}
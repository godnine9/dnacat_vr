﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Data;
using System.IO;
using UnityEngine;
using UnityEditor;
using ExcelDataReader;

namespace Ninth
{
	public class DataParser
	{
		private const string handleDataAfterReimportKey = "handleDataAfterReimport";

		[MenuItem("Ninth/DataParser/Parse Code")]
		public static void ParseCode()
		{
			ReadExcelFiles(HandleCode);
		}

		[MenuItem("Ninth/DataParser/Parse Data")]
		public static void ParseData()
		{
			ReadExcelFiles(HandleData);
		}

		[MenuItem("Ninth/DataParser/Parse All")]
		public static void ParseAll()
		{
			EditorPrefs.SetBool(handleDataAfterReimportKey, true);
			ReadExcelFiles(HandleCode, HandleData);
		}

		private static void ReadExcelFiles(params Func<DataTable, bool>[] onHandleTable)
		{
			if (onHandleTable == null)
			{
				Debug.LogError("Action to handle data table can not be null.");
				return;
			}

			string excelPath = GetExcelPath();
			if (!Directory.Exists(excelPath))
			{
				Debug.LogError("Directory not exists to parse data.");
				return;
			}

			var dataPaths = Directory.GetFiles(excelPath, DataParserConfig.ExcelFileExtension);
			for (int i = 0; i < dataPaths.Length; ++i)
			{
#pragma warning disable IDE0063
				using (var stream = File.Open(dataPaths[i], FileMode.Open, FileAccess.Read))
				{
					using (var reader = ExcelReaderFactory.CreateOpenXmlReader(stream))
					{
						var dataSet = reader.AsDataSet();
						for (int j = 0; j < dataSet.Tables.Count; ++j)
						{
							var table = dataSet.Tables[j];
							if (table.TableName.StartsWith(DataParserConfig.TableIgnoreSign))
							{
								continue;
							}

							for (int k = 0; k < onHandleTable.Length; ++k)
							{
								var onHandle = onHandleTable[k];
								if (onHandle == null)
								{
									continue;
								}

								if (!onHandle.Invoke(table))
								{
									Debug.LogError($"<color=red>[{nameof(DataParser)}.{onHandle.GetMethodInfo().Name}] Table<{table.TableName}> failure</color>");
									continue;
								}

								Debug.Log($"<color=green>[{nameof(DataParser)}.{onHandle.GetMethodInfo().Name}] Table<{table.TableName}> success</color>");
								AssetDatabase.SaveAssets();
								AssetDatabase.Refresh();
							}
						}
					}
				}
#pragma warning restore IDE0063
			}
		}

		private static bool HandleData(DataTable table)
		{
			var rowsData = table.Rows;
			int columns = table.Columns.Count;
			int rows = rowsData.Count;

			string dataName = $"{table.TableName}{DataParserConfig.Instance.DataSuffix}";
			string dataSetName = $"{table.TableName}{DataParserConfig.Instance.DataSetSuffix}";

			string dataTypeName = $"{DataParserConfig.Instance.Namespace}.{dataName},{DataParserConfig.DataAssembly}";
			string dataSetTypeName = $"{DataParserConfig.Instance.Namespace}.{dataSetName},{DataParserConfig.DataAssembly}";

			Type elementDataType = Type.GetType(dataTypeName);
			Type dataSetType = Type.GetType(dataSetTypeName);

			if (elementDataType == null)
			{
				Debug.LogError($"Type not found: {dataTypeName}");
				return false;
			}

			if (dataSetType == null)
			{
				Debug.LogError($"Type not found: {dataSetTypeName}");
				return false;
			}

			string path = Path.Combine(DataParserConfig.Instance.DataSavePath, $"{dataSetName}.asset").Replace('\\', '/');
			var asset = AssetDatabase.LoadAssetAtPath(path, dataSetType);

			if (!asset)
			{
				asset = ScriptableObject.CreateInstance(dataSetName);
				if (!asset)
				{
					return false;
				}
				AssetDatabase.CreateAsset(asset, path);
			}

			var elementListFieldInfo = dataSetType.BaseType.GetField("data", BindingFlags.NonPublic | BindingFlags.Instance);
			var elementList = (IList)Activator.CreateInstance(typeof(List<>).MakeGenericType(elementDataType), 0);

			var idHashSet = new HashSet<int>();

			// Handle Value
			for (int row = 3; row < rows; ++row)
			{
				if (rowsData[row][0].ToString().StartsWith(DataParserConfig.FieldIgnoreSign))
				{
					continue;
				}

				bool invalidID = false;
				var element = Activator.CreateInstance(elementDataType);

				// { fieldName, (fieldTypeName, index, Length, arg) }
				var arrayFields = new Dictionary<string, (string, int, object[])>();

				for (int col = 1; col < columns; ++col)
				{
					if (rowsData[0][col].ToString().StartsWith(DataParserConfig.FieldIgnoreSign))
					{
						continue;
					}

					string fieldName = rowsData[1][col].ToString();
					string fieldTypeNameDef = rowsData[2][col].ToString();

					if (!DataParserConfig.FieldTypeMap.TryGetValue(fieldTypeNameDef, out string fieldTypeName))
					{
						continue;
					}

					var fieldType = Type.GetType(fieldTypeName);
					if (fieldType == null)
					{
						Debug.LogWarning($"Type not defined: {fieldType}");
						continue;
					}

					var converter = TypeDescriptor.GetConverter(fieldType);
					if (converter == null)
					{
						Debug.LogWarning($"Type converter not defined: {fieldType}");
						continue;
					}

					var data = Convert.ToString(rowsData[row][col], DataParserConfig.CultureFormat);

					int id = 0;
					if (fieldName.Equals(DataParserConfig.Instance.KeyFieldName))
					{
						if (string.IsNullOrEmpty(data) || !int.TryParse(data, out id) || id < 1)
						{
							invalidID = true;
							Debug.LogWarning($"Invalid ID detected: {data}");
							continue;
						}

						if (idHashSet.Contains(id))
						{
							invalidID = true;
							Debug.LogWarning($"Duplicate ID detected: {data}");
							continue;
						}
					}
					idHashSet.Add(id);

					object arg;
					try
					{
						if (!fieldType.Equals("string"))
						{
							arg = converter.ConvertFromString(data);
						}
						else
						{
							arg = data;
						}
					}
					catch (Exception e)
					{
						string tableLocation = $"Table:{table.TableName} [{row}, {col}]";
						Debug.LogWarning($"{e.Message}\n{tableLocation}");
						continue;
					}

					// Detect whether field is an array.
					if (DataParserUtility.TryGetEndUnsignedNumber(fieldName, out int number))
					{
						fieldName = fieldName.Replace(number.ToString(), string.Empty);
						int index = number - 1;
						if (index >= 0)
						{
							int arrayLength = number;
							if (arrayFields.TryGetValue(fieldName, out var arrayFieldInfo))
							{
								string arrayFieldType = arrayFieldInfo.Item1;
								if (!arrayFieldType.Equals(fieldTypeName))
								{
									Debug.LogError($"The table '{table.TableName}' includes array field'{fieldName}[]' with same field name but different field type is invalid.");
									continue;
								}
								arrayLength = Math.Max(arrayLength, arrayFieldInfo.Item3.Length);
							}

							object[] arrayElements = arrayFieldInfo.Item3;
							if (arrayElements == null)
							{
								arrayElements = new object[arrayLength];
							}
							else
							{
								Array.Resize(ref arrayElements, arrayLength);
							}
							arrayElements[index] = arg;
							arrayFields[fieldName] = (fieldTypeName, index, arrayElements);
						}
						continue;
					}

					var elementFieldInfo = elementDataType.GetField(fieldName);
					if (elementFieldInfo == null)
					{
						Debug.LogWarning($"Field name not match: {fieldTypeName} x {fieldName}");
						continue;
					}

					if (!elementFieldInfo.FieldType.Equals(fieldType))
					{
						Debug.LogWarning($"Field type not match: [{fieldTypeName}] x [{elementFieldInfo.FieldType}]");
						continue;
					}

					if (!arg.GetType().Equals(elementFieldInfo.FieldType))
					{
						Debug.LogError($"Arguement '{arg}' type {arg.GetType()} not match to {elementFieldInfo.FieldType}.\nTable:{table.TableName} [{row}, {col}]");
						continue;
					}
					
					elementFieldInfo.SetValue(element, arg);
				}

				// Handle array field...
				foreach (var arrayField in arrayFields)
				{
					string fieldName = arrayField.Key;
					string fieldTypeName = arrayField.Value.Item1;
					int index = arrayField.Value.Item2;
					var args = arrayField.Value.Item3;

					var elementFieldInfo = elementDataType.GetField(fieldName);
					if (elementFieldInfo == null)
					{
						Debug.LogWarning($"Field name not match: {fieldTypeName} x {fieldName}");
						continue;
					}

					var array = Array.CreateInstance(Type.GetType(fieldTypeName), args.Length);
					for (int i = 0; i < args.Length; ++i)
					{
						array.SetValue(args[i], i);
					}
					elementFieldInfo.SetValue(element, array);
				}

				if (invalidID)
				{
					continue;
				}

				elementList.Add(element);
			}

			elementListFieldInfo.SetValue(asset, elementList);
			EditorUtility.SetDirty(asset);
			return true;
		}

		private static bool HandleCode(DataTable table)
		{
			var rowsData = table.Rows;
			int columns = table.Columns.Count;

			string dataName = $"{table.TableName}{DataParserConfig.Instance.DataSuffix}";
			string dataSetName = $"{table.TableName}{DataParserConfig.Instance.DataSetSuffix}";

			var fields = new List<(string, string)>();  // (FieldType, FieldName)

			var arrayFieldNames = new HashSet<string>();
			for (int col = 1; col < columns; ++col)
			{
				if (rowsData[0][col].ToString().StartsWith(DataParserConfig.FieldIgnoreSign))
				{
					continue;
				}

				string fieldName = rowsData[1][col].ToString();
				string fieldTypeDef = rowsData[2][col].ToString();

				if (string.IsNullOrEmpty(fieldName) || string.IsNullOrEmpty(fieldTypeDef))
				{
					continue;
				}

				if (DataParserConfig.FieldTypeMap.TryGetValue(fieldTypeDef, out string fieldType))
				{
					if (DataParserUtility.TryGetEndUnsignedNumber(fieldName, out int number))
					{
						fieldType = $"{fieldType}[]";
						fieldName = fieldName.Replace(number.ToString(), string.Empty);

						if (arrayFieldNames.Contains(fieldName))
						{
							continue;
						}
						arrayFieldNames.Add(fieldName);
					}
					fields.Add((fieldType, fieldName));
				}
				else
				{
					Debug.LogError($"Field type definition<{fieldTypeDef}> in excel is not found.");
				}
			}

			string dataSavePath = Path.Combine(DataParserConfig.Instance.CodeSavePath, $"{dataName}.cs");
			string dataSetSavePath = Path.Combine(DataParserConfig.Instance.CodeSavePath, $"{dataSetName}.cs");

			bool dataSuccess = DataParserUtility.MakeDataCode(dataName, fields.ToArray(), dataSavePath);
			if (!dataSuccess)
			{
				Debug.LogError($"Generate Data failed: {dataName}");
			}

			bool dataSetSuccess = DataParserUtility.MakeDataSetCode(dataSetName, dataName, dataSetSavePath);
			if (!dataSetSuccess)
			{
				Debug.LogError($"Generate DataSet failed: {dataSetName}");
			}

			return dataSuccess && dataSetSuccess;
		}

		[UnityEditor.Callbacks.DidReloadScripts]
		private static void OnRecompiled()
		{
			if (EditorPrefs.GetBool(handleDataAfterReimportKey, true))
			{
				ReadExcelFiles(HandleData);
				EditorPrefs.SetBool(handleDataAfterReimportKey, false);
			}
		}

		private static string GetExcelPath()
		{
			string projectPath = Directory.GetParent(Application.dataPath).FullName;
			string rootPath = Directory.GetParent(projectPath).FullName;
			return Path.Combine(rootPath, DataParserConfig.Instance.ExcelPath);
		}
	}
}

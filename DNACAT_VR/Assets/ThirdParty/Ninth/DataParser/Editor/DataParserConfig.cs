﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.CodeDom.Compiler;
using UnityEditor;

namespace Ninth
{
	[ConfigPath("Assets/ThirdParty/Ninth/DataParser/Editor/DataParserConfig.asset")]
	public class DataParserConfig : EditorConfig<DataParserConfig>
	{
		public static readonly IFormatProvider CultureFormat = CultureInfo.InvariantCulture;
		public static readonly CodeDomProvider DOMProvider = CodeDomProvider.CreateProvider("CSharp");
		public static readonly CodeGeneratorOptions DOMOptions = new CodeGeneratorOptions { BracingStyle = "C" };

		public const string TableIgnoreSign = "_";
		public const string FieldIgnoreSign = "#";
		public const string ExcelFileExtension = "*.xlsx";
		public const string DataAssembly = "Assembly-CSharp";
		public const string TypeName_Int32 = "System.Int32";
		public const string TypeName_String = "System.String";
		public const string TypeName_DateTime = "System.DateTime";

		public string ExcelPath = @"Doc\Table";
		public string CodeSavePath = "Assets/ThirdParty/Ninth/DataParser/Data";
		public string DataSavePath = "Assets/ThirdParty/Ninth/DataParser/Resources/Data";
		public string Namespace = "Ninth";
		public string DataSuffix = "Data";
		public string DataSetSuffix = "DataSet";
		public string KeyFieldName = "ID";

		public static readonly IReadOnlyDictionary<string, string> FieldTypeMap = new Dictionary<string, string>
		{
			{ "int", TypeName_Int32 },
			{ "string", TypeName_String },
			{ "timestamp", TypeName_DateTime },
		};

#pragma warning disable IDE0051
		[MenuItem("Ninth/DataParser/Create DataParserConfig")]
		private static void CreateConfigInstanceMenuItem()
		{
			CreateConfigInstance();
		}
#pragma warning restore IDE0051
	}
}

﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using UnityEditor.U2D;

namespace Ninth
{
	public class SpriteAtlasPacker : ScriptableWizard
	{
		private const string windowTitle = "SpriteAtlasPacker";

		[SerializeField]
		private string atlasName = "Atlas";
		[SerializeField]
		private string fileExtension = "spriteatlas";
		[SerializeField]
		private string savePath = "Assets/UI/Atlas";
		[SerializeField]
		private int padding = 4;
		[SerializeField]
		private bool enableRotation = true;
		[SerializeField]
		public bool enableTightPacking = false;
		[SerializeField]
		private Texture2D[] textures = null;

		[MenuItem("Ninth/UI/SpriteAtlasPacker", priority = 0)]
		public static void WiazrdWindow()
		{
			DisplayWizard<SpriteAtlasPacker>(windowTitle);
		}

		[MenuItem("Ninth/UI/PackAllAtlases", priority = 1)]
		public static void PackPreviewAllAtlases()
		{
			SpriteAtlasUtility.PackAllAtlases(EditorUserBuildSettings.activeBuildTarget, false);
		}

		private void OnWizardCreate()
		{
			var atlas = new SpriteAtlas();
			for (int i = 0; i < textures.Length; ++i)
			{
				string path = AssetDatabase.GetAssetPath(textures[i]);
				var assets = AssetDatabase.LoadAllAssetsAtPath(path).Where(asset => asset.GetType().Equals(typeof(Sprite))).ToArray();
				var packingSetting = new SpriteAtlasPackingSettings
				{
					padding = padding,
					enableRotation = enableRotation,
					enableTightPacking = enableTightPacking,
				};
				atlas.SetPackingSettings(packingSetting);
				atlas.Add(assets);
			}

			AssetDatabase.CreateAsset(atlas, $"{savePath}/{atlasName}.{fileExtension}");
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
			EditorGUIUtility.PingObject(atlas);
		}
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace Ninth
{
	public class LocalizeManager : MonoBehaviour
	{
		private HashSet<ILocalizeHandler> localizeHandlers = null;

		private static LocalizeManager instance = null;

		public static LocalizeManager Instance
		{
			get
			{
				if (instance == null)
				{
					Debug.LogWarning($"Singleton '{nameof(LocalizeManager)}' is not set as an instance in scene.");
				}
				return instance;
			}
		}

		public void Add(ILocalizeHandler handler)
		{
			localizeHandlers.Add(handler);
		}

		public void NotifyTextsRefresh()
		{
			foreach (var handler in localizeHandlers)
			{
				handler.Refresh();
			}
		}

		public void Remove(ILocalizeHandler handler)
		{
			if (!localizeHandlers.Remove(handler))
			{
#if UNITY_EDITOR
				Debug.LogWarning($"ILocalizeHandler not included in handlers.");
#endif
			}
		}

		private void Awake()
		{
			if (instance == null)
			{
				instance = this;
				DontDestroyOnLoad(gameObject);
				instance.localizeHandlers = new HashSet<ILocalizeHandler>();
			}
		}
	}
}
﻿using Ninth;
using UnityEngine;

namespace Ninth
{
	public static class LocalizeUtility
	{
		public static string GetLocalize(int key)
		{
			var instance = LocalizationDataSet.Instance;
			if (instance == null)
			{
				Debug.LogError($"{nameof(LocalizationDataSet)} is not loaded yet.");
				return string.Empty;
			}

			if (!instance.TryGetData(key, out var data))
			{
				Debug.LogError($"Localization data not found. ID:{key}");
				return string.Empty;
			}

			switch (GetCurrentLanguage())
			{
				case Language.zhTW:
					return data.TWN;
				case Language.zhCN:
					return data.CHS;
				case Language.enUS:
					return data.EN;
				case Language.JA:
					return data.JP;
				default:
					return string.Empty;
			}
		}

		public static void SetLanguage(Language language)
		{
			LocalizeConfig.Instance.SaveLanguage(language);
 			LocalizeManager.Instance.NotifyTextsRefresh();
		}

		public static Language GetCurrentLanguage()
		{
			return LocalizeConfig.Instance.CurrentLanguage;
		}
	}
}

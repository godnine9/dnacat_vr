﻿using UnityEngine;
using UnityEngine.UI;

namespace Ninth
{
	public class LocalizeText : Text, ILocalizeHandler
	{
		[SerializeField]
		private int localizeID = 0;

		[SerializeField]
		private bool refreshOnAwake = true;

		public int LocalizeID => localizeID;

		public bool RefreshOnAwake => refreshOnAwake;

		public void Refresh()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				return;
			}
#endif
			text = LocalizeUtility.GetLocalize(localizeID);
		}

		public void SetLocalizeID(int id, bool refresh = true)
		{
			localizeID = id;
			if (refresh)
			{
				Refresh();
			}
		}

		protected override void OnEnable()
		{
			base.OnEnable();
			if (LocalizeManager.Instance != null)
			{
				LocalizeManager.Instance.Add(this);
			}

			if (refreshOnAwake)
			{
				Refresh();
			}
		}

		protected override void OnDisable()
		{
			base.OnDisable();
			if (LocalizeManager.Instance != null)
			{
				LocalizeManager.Instance.Remove(this);
			}
		}

#if UNITY_EDITOR
		protected override void Reset()
		{
			base.Reset();
			raycastTarget = false;
			maskable = false;
			refreshOnAwake = true;
		}
#endif
	}
}

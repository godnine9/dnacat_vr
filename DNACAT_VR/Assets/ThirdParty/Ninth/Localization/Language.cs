﻿namespace Ninth
{
	public enum Language : int
	{
		zhTW,
		zhCN,
		enUS,
		JA,
		Ca,
		Kr,
		Ru,
		Fr,
	}
}

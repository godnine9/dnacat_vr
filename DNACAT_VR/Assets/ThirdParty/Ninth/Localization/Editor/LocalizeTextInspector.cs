﻿using UnityEditor;
using UnityEngine;

namespace Ninth
{
	[CustomEditor(typeof(LocalizeText), true)]
	public class LocalizeTextInspector : UnityEditor.UI.TextEditor
	{
		private SerializedProperty m_TextSP = null;
		private SerializedProperty m_fontDataSP = null;

		private bool foldout = true;
		private LocalizeEditorHandler handler = null;

		protected override void OnEnable()
		{
			base.OnEnable();
			m_TextSP = serializedObject.FindProperty("m_Text");
			m_fontDataSP = serializedObject.FindProperty("m_FontData");
			handler = new LocalizeEditorHandler(target as LocalizeText, serializedObject);
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			EditorGUILayout.PropertyField(m_TextSP);
			EditorGUILayout.PropertyField(m_fontDataSP);
			AppearanceControlsGUI();
			RaycastControlsGUI();
			MaskableControlsGUI();

			foldout = EditorGUILayout.BeginFoldoutHeaderGroup(foldout, "Localization");
			if (foldout)
			{
				EditorGUI.indentLevel++;
				handler?.OnInspectorGUI();
				EditorGUI.indentLevel--;
			}
			EditorGUILayout.EndFoldoutHeaderGroup();
			serializedObject.ApplyModifiedProperties();
		}

		public override void DrawPreview(Rect previewArea)
		{
			handler?.DrawPreview(previewArea);
		}

		public override bool HasPreviewGUI()
		{
			return true;
		}
	}
}
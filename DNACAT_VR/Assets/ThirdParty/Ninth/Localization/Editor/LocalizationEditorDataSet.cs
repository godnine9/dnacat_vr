﻿using UnityEditor;

namespace Ninth
{
	public class LocalizationEditorDataSet : EditorDataSet<LocalizationData>
	{
		[InitializeOnLoadMethod]
		public static void OnRecompiled()
		{ 
			Load(nameof(LocalizationDataSet));
		}
	}
}
﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Ninth;

namespace Ninth
{
	public class LocalizeEditorHandler
	{
		private readonly Text target = null;
		private readonly SerializedObject serializedObject = null;
		private readonly SerializedProperty localizeIDSP = null;
		private readonly SerializedProperty refreshOnAwakeSP = null;
		private GUIStyle previewGUILabelStyle = null;

		public int LocalizeID
		{
			get => localizeIDSP.intValue;
			set => localizeIDSP.intValue = value;
		}

		public LocalizeEditorHandler(Text target, SerializedObject serializedObject)
		{
			this.target = target;
			this.serializedObject = serializedObject;
			localizeIDSP = serializedObject.FindProperty("localizeID");
			refreshOnAwakeSP = serializedObject.FindProperty("refreshOnAwake");
			SetupTextPreviewSettings();
			RefreshTextValue();
		}

		public void OnInspectorGUI()
		{
			EditorGUILayout.Space(3);
			EditorGUI.BeginChangeCheck();
			EditorGUILayout.PropertyField(refreshOnAwakeSP);
			LocalizeID = Mathf.Clamp(EditorGUILayout.DelayedIntField(nameof(LocalizeID), LocalizeID), LocalizationEditorDataSet.MinID, LocalizationEditorDataSet.MaxID);

			EditorGUILayout.Space(3);
			if (GUILayout.Button("Reload SystemEditorPreset"))
			{
				LocalizationEditorDataSet.OnRecompiled();
			}

			if (EditorGUI.EndChangeCheck())
			{
				Undo.RecordObject(target.gameObject, "Edit LocalizeText");
				EditorUtility.SetDirty(target.gameObject);
				serializedObject.ApplyModifiedProperties();
				SetupTextPreviewSettings();
				RefreshTextValue();
			}
		}

		public void DrawPreview(Rect previewArea) 
		{
			EditorGUI.DrawTextureTransparent(previewArea, Texture2D.grayTexture);
			if (previewGUILabelStyle != null)
			{
				GUI.Label(previewArea, target.text, previewGUILabelStyle);
			}
		}

		private void RefreshTextValue()
		{
			if (LocalizationEditorDataSet.DataContainer != null && LocalizationEditorDataSet.DataContainer.TryGetValue(LocalizeID, out var data))
			{
				switch (LocalizeConfig.Instance.DefaultLanguage)
				{
					case Language.zhTW:
						target.text = data.TWN;
						break;
					case Language.zhCN:
						target.text = data.CHS;
						break;
					case Language.enUS:
						target.text = data.EN;
						break;
					case Language.JA:
						target.text = data.JP;
						break;
					default:
						target.text = string.Empty;
						break;
				}
			}
			else
			{
				target.text = string.Empty;
			}
		}

		private void SetupTextPreviewSettings()
		{
			previewGUILabelStyle = new GUIStyle
			{
				font = target.font,
				fontSize = 50,
				fontStyle = target.fontStyle,
			};
			previewGUILabelStyle.normal.textColor = target.color;
			previewGUILabelStyle.alignment = TextAnchor.MiddleCenter;
		}
	}
}

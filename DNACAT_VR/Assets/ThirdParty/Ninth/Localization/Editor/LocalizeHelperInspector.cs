﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Ninth
{
	[CanEditMultipleObjects]
	[CustomEditor(typeof(LocalizeHelper), true)]
    public class LocalizeHelperInspector : Editor
    {
		private LocalizeHelper component = null;
		private SerializedProperty textSP = null;

		private LocalizeEditorHandler handler = null;

		private Text Text
		{
			get => textSP.objectReferenceValue as Text;
			set => textSP.objectReferenceValue = value;
		}

		private void OnEnable()
		{
			component = target as LocalizeHelper;
			textSP = serializedObject.FindProperty("text");

			if (Text)
			{
				handler = new LocalizeEditorHandler(Text, serializedObject);
			}
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			if (!Text)
			{
				EditorGUILayout.HelpBox("Text component has not been asigned yet.", MessageType.Error);
				if (GUILayout.Button("Asign Text component"))
				{
					if (component.gameObject.TryGetComponent<Text>(out var textComponent))
					{
						Text = textComponent;
						handler = new LocalizeEditorHandler(Text, serializedObject);
						Undo.RecordObject(target, "Asign Text component");
						EditorUtility.SetDirty(target);
					}
				}
				serializedObject.ApplyModifiedProperties();
				return;
			}

			handler?.OnInspectorGUI();
			serializedObject.ApplyModifiedProperties();
		}

		public override void DrawPreview(Rect previewArea)
		{
			handler?.DrawPreview(previewArea);
		}

		public override bool HasPreviewGUI()
		{
			return true;
		}
	}
}

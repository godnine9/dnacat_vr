﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using Ninth;

namespace Ninth
{
	[InitializeOnLoad]
	public static class LocalizeTextInspectorResolver
	{
		private const double refreshFreq = 1;
		private static double nextRefreshTime = 0;
		private static HashSet<LocalizeText> targets = null;

		public static bool IsInit
		{
			get;
			private set;
		}

		static LocalizeTextInspectorResolver()
		{
			EditorApplication.update += Refresh;
			EditorApplication.hierarchyChanged += Refresh;
			EditorSceneManager.sceneOpened += OnSceneOpened;
			EditorSceneManager.sceneSaved += OnSceneSaved;
		}

		private static void Refresh()
		{
			if (nextRefreshTime > EditorApplication.timeSinceStartup || EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}
			SetNextRefreshTime();

			if (!IsInit)
			{
				FindAllTexts();
				IsInit = true;
			}

			if (targets != null && LocalizationEditorDataSet.DataContainer != null)
			{
				foreach (var target in targets) 
				{
					if (LocalizationEditorDataSet.DataContainer.TryGetValue(target.LocalizeID, out var data))
					{
						switch (LocalizeConfig.Instance.CurrentLanguage)
						{
							case Language.zhTW:
								target.text = data.TWN;
								break;
							case Language.zhCN:
								target.text = data.CHS;
								break;
							case Language.enUS:
								target.text = data.EN;
								break;
							case Language.JA:
								target.text = data.JP;
								break;
							default:
								target.text = string.Empty;
								break;
						}
					}
				}
			}
		}

		private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
		{
			FindAllTexts();
		}

		private static void OnSceneSaved(Scene scene)
		{
			FindAllTexts();
		}

		private static void FindAllTexts()
		{
			targets = new HashSet<LocalizeText>(Resources.FindObjectsOfTypeAll<LocalizeText>());
		}

		private static void SetNextRefreshTime()
		{
			nextRefreshTime = EditorApplication.timeSinceStartup + refreshFreq;
		}
	}
}

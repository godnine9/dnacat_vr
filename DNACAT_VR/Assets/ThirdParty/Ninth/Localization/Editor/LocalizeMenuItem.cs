﻿using UnityEngine;
using UnityEditor;

namespace Ninth
{
	public class LocalizeMenuItem
	{
		[MenuItem("GameObject/UI/Mahua/LocalizeText")]
		public static LocalizeText CreateLocalizeText()
		{
			var gameObject = new GameObject(nameof(LocalizeText))
			{
				layer = LayerMask.NameToLayer("UI")
			};

			var localizeText = gameObject.AddComponent<LocalizeText>();

			if (EditorHierarchyHelper.TryGetCurrentSelectionAsParent<Canvas>(out var parent))
			{
				gameObject.transform.SetParent(parent);
			}

			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.transform.localScale = Vector3.one;

			var rectTransform = gameObject.transform as RectTransform;
			if (rectTransform)
			{
				rectTransform.anchoredPosition = Vector2.zero;
			}

			Selection.activeGameObject = gameObject;
			return localizeText;
		}
	}
}

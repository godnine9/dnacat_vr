﻿namespace Ninth
{
	public interface ILocalizeHandler
	{
		int LocalizeID { get; }

		bool RefreshOnAwake { get; }

		void Refresh();
	}
}

﻿using UnityEngine;

namespace Ninth
{
	[ConfigPath("Config/LocalizeConfig")]
	public class LocalizeConfig : Config<LocalizeConfig>
	{
		public Language DefaultLanguage = default;
		public string PlayerPrefKey = "Localization";

		public Language CurrentLanguage => (Language)PlayerPrefs.GetInt(PlayerPrefKey, (int)DefaultLanguage);

		public void SaveLanguage(Language language)
		{
			PlayerPrefs.SetInt(PlayerPrefKey, (int)language);
			PlayerPrefs.Save();
		}
	}
}

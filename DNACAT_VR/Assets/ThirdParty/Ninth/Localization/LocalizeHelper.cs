﻿using UnityEngine;
using UnityEngine.UI;

namespace Ninth
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Text))]
	public class LocalizeHelper : MonoBehaviour, ILocalizeHandler
	{
		[SerializeField]
		private int localizeID = 0;

		[SerializeField]
		private bool refreshOnAwake = true;

		[SerializeField, HideInInspector]
		private Text text = null;

		public int LocalizeID => localizeID;

		public bool RefreshOnAwake => refreshOnAwake;

		private void Awake()
		{
			if (refreshOnAwake)
			{
				Refresh();
			}
		}

		public void Refresh()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				return;
			}
#endif
			if (text)
			{
				text.text = LocalizeUtility.GetLocalize(localizeID);
			}
		}

		private void Reset()
		{
			refreshOnAwake = false;
			TryGetComponent(out text);
		}
	}
}

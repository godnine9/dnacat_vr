﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

namespace Ninth
{
	public sealed partial class AtlasImage : Image
	{
		[SerializeField]
		private SpriteAtlas spriteAtlas = null;

		[SerializeField]
		private string spriteName = string.Empty;

		public SpriteAtlas SpriteAtlas => spriteAtlas;

		public void SetAtlas(SpriteAtlas spriteAtlas)
		{
			this.spriteAtlas = spriteAtlas;
		}

		public void SetSprite(string spriteName)
		{
			this.spriteName = spriteName;
			sprite = spriteAtlas.GetSprite(spriteName);
		}

		protected override void Awake()
		{
#if UNITY_EDITOR
			if (Application.isPlaying)
			{
#endif
				SetSprite(spriteName);
#if UNITY_EDITOR
			}
#endif
			base.Awake();
		}

		protected override void OnEnable()
		{
			base.OnEnable();
#if UNITY_EDITOR
			if (spriteAtlas != null && !Application.isPlaying)
			{
				overrideSprite = spriteAtlas.GetSprite(spriteName);
			}
#endif
		}

		protected override void OnDestroy()
		{
#if UNITY_EDITOR
			overrideSprite = null;
#endif
			sprite = null;
			base.OnDestroy();
		}

#if UNITY_EDITOR
		public Action OnReset = null;
		protected override void Reset()
		{
			sprite = null;
			overrideSprite = null;
			spriteAtlas = null;
			spriteName = string.Empty;
			raycastTarget = false;
			maskable = false;
			color = Color.white;
			material = null;
			type = Type.Simple;
			rectTransform.sizeDelta = new Vector2(100f, 100f);
			OnReset?.Invoke();
			base.Reset();
		}
#endif

#if UNITY_EDITOR
		public void Refresh()
		{
			if (!overrideSprite && spriteAtlas)
			{
				overrideSprite = spriteAtlas.GetSprite(spriteName);
			}
		}
#endif
	}
}
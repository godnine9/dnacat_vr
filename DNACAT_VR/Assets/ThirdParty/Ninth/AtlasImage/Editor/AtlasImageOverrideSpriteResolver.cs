﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Ninth
{
	[InitializeOnLoad]
	public static class AtlasImageOverrideSpriteResolver
	{
		private const double refreshFreq = 1;
		private static double nextRefreshTime = 0;

		private static HashSet<AtlasImage> _images = null;

		static AtlasImageOverrideSpriteResolver()
		{
			EditorApplication.update += Refresh;
			EditorApplication.hierarchyChanged += Refresh;
			EditorSceneManager.sceneOpened += OnSceneOpened;
			EditorSceneManager.sceneSaved += OnSceneSaved;
		}

		public static bool IsInit
		{
			get;
			private set;
		}

		public static void Refresh()
		{
			if (nextRefreshTime > EditorApplication.timeSinceStartup || EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			if (!IsInit)
			{
				FindAllImages();
				IsInit = true;
			}

			foreach (var image in _images)
			{
				if (image)
				{
					image.Refresh();
				}
			}
			SetNextRefreshTime();
		}

		private static void SetNextRefreshTime()
		{
			nextRefreshTime = EditorApplication.timeSinceStartup + refreshFreq;
		}

		private static void OnSceneOpened(Scene scene, OpenSceneMode mode)
		{
			FindAllImages();
		}

		private static void OnSceneSaved(Scene scene)
		{
			FindAllImages();
		}

		private static void FindAllImages()
		{
			_images?.Clear();
			_images = new HashSet<AtlasImage>();
			var buttonImages = Array.ConvertAll(Resources.FindObjectsOfTypeAll<Button>(), button => button.image as AtlasImage);
			var toggles = Resources.FindObjectsOfTypeAll<Toggle>();
			var toggleCheckImages = toggles.Select(toggle => toggle.graphic as AtlasImage);
			var toggleImages = Array.ConvertAll(toggles, toggle => toggle.image as AtlasImage);
			_images.UnionWith(buttonImages);
			_images.UnionWith(toggleCheckImages);
			_images.UnionWith(toggleImages);
		}
	}
}
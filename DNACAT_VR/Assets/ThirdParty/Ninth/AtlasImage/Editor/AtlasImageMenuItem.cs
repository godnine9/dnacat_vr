﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Ninth
{
	public class AtlasImageMenuItem
	{
		[MenuItem("GameObject/UI/Mahua/AtlasImage")]
		public static AtlasImage CreateAtlasImage()
		{
			var atlasImage = CreateAtlasImageInstance(nameof(AtlasImage), false);
			Selection.activeGameObject = atlasImage.transform.gameObject;
			return atlasImage;
		}

		[MenuItem("GameObject/UI/Mahua/AtlasButton")]
		public static Button CreateAtlasButton()
		{
			var atlasImage = CreateAtlasImageInstance("AtlasButton", true);
			Selection.activeGameObject = atlasImage.gameObject;
			return atlasImage.gameObject.AddComponent<Button>();
		}

		[MenuItem("GameObject/UI/Mahua/AtlasToggle")]
		public static Toggle CreateAtlasToggle()
		{
			var atlasImage = CreateAtlasImageInstance("AtlasToggle", true);
			Selection.activeGameObject = atlasImage.gameObject;
			return atlasImage.gameObject.AddComponent<Toggle>();
		}

		private static AtlasImage CreateAtlasImageInstance(string name, bool raycastTarget)
		{
			var gameObject = new GameObject(name)
			{
				layer = LayerMask.NameToLayer("UI")
			};

			var atlasImage = gameObject.AddComponent<AtlasImage>();
			atlasImage.raycastTarget = raycastTarget;

			if (EditorHierarchyHelper.TryGetCurrentSelectionAsParent<Canvas>(out var parent))
			{
				gameObject.transform.SetParent(parent);
			}

			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			gameObject.transform.localScale = Vector3.one;

			var rectTransform = gameObject.transform as RectTransform;
			if (rectTransform)
			{
				rectTransform.anchoredPosition = Vector2.zero;
			}

			AtlasImageOverrideSpriteResolver.Refresh();
			return atlasImage;
		}
	}
}
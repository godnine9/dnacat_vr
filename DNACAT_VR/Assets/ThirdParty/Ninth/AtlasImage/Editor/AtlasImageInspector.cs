﻿using System;
using UnityEngine;
using UnityEngine.U2D;
using UnityEditor;
using UnityEditor.UI;

namespace Ninth
{
	[CanEditMultipleObjects, CustomEditor(typeof(AtlasImage))]
	public sealed class AtlasImageInspector : ImageEditor
	{
		private const string fieldLabelSpirteAtlas = "Sprite Atlas";
		private const string fieldLabelSpirte = "Sprite";
		private const string spriteNamesDisplayReplace = "(Clone)";
		private const float previewGUILabelHeightPadding = 35f;
		private readonly GUIStyle previewGUILabelStyle = new GUIStyle();

		private AtlasImage atlasImage = null;
		private SerializedProperty atlasSP = null;
		private SerializedProperty spriteNameSP = null;
		private Sprite[] sprites = null;
		private string[] spriteNames = null;
		private int currentSpriteIndex = 0;

		private SpriteAtlas SpriteAtlas
		{
			get => atlasSP.objectReferenceValue as SpriteAtlas;
			set => atlasSP.objectReferenceValue = value;
		}

		private string spriteName
		{
			get => spriteNameSP.stringValue;
			set => spriteNameSP.stringValue = value;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			#region Draw Inspector
			DrawEditableField(OnShowAtlasField, isChanged =>
			{
				if (isChanged)
				{
					RefreshSpriteNames();
				}
			});

			DrawEditableField(OnShowSpriteNameField, isChanged =>
			{
				if (isChanged)
				{
					spriteName = spriteNames[currentSpriteIndex];
					atlasImage.overrideSprite = sprites[currentSpriteIndex];
				}
			});

			// Color & Material
			AppearanceControlsGUI();
			RaycastControlsGUI();
			MaskableControlsGUI();

			if (atlasImage.overrideSprite != null)
			{
				TypeGUI();
				NativeSizeButtonGUI();
			}
			#endregion Draw Inspector

			serializedObject.ApplyModifiedProperties();
		}

		protected override void OnEnable()
		{
			previewGUILabelStyle.fontSize = 15;
			previewGUILabelStyle.fontStyle = FontStyle.Bold;
			previewGUILabelStyle.normal.textColor = Color.white;
			previewGUILabelStyle.alignment = TextAnchor.MiddleCenter;

			atlasImage = target as AtlasImage;
			atlasImage.OnReset = OnReset;
			atlasSP = serializedObject.FindProperty(nameof(SpriteAtlas));
			spriteNameSP = serializedObject.FindProperty(nameof(spriteName));
			RefreshSpriteNames();
			base.OnEnable();
			SetShowNativeSize(true, true);
		}

		public override void DrawPreview(Rect previewArea)
		{
			if (!atlasImage.overrideSprite)
			{
				return;
			}

			EditorGUI.DrawTextureTransparent(previewArea, atlasImage.overrideSprite.texture, ScaleMode.ScaleToFit, 0, -1);
			Rect labelPosition = new Rect(new Vector2(previewArea.width * 0.5f, previewArea.height), Vector2.zero);
			EditorGUI.DropShadowLabel(labelPosition, $"Image Size:{atlasImage.overrideSprite.texture.width}x{atlasImage.overrideSprite.texture.height}", previewGUILabelStyle);
			labelPosition.height -= previewGUILabelHeightPadding;
			EditorGUI.DropShadowLabel(labelPosition, atlasImage.gameObject.name, previewGUILabelStyle);
		}

		protected override void OnDisable()
		{
			atlasImage.OnReset = null;
			base.OnDisable();
		}

		private void RefreshSpriteNames()
		{
			if (SpriteAtlas)
			{
				sprites = new Sprite[SpriteAtlas.spriteCount];
				SpriteAtlas.GetSprites(sprites);

				if (sprites.Length != 0)
				{
					Array.Sort(sprites, (a, b) => a.name.CompareTo(b.name));
				}
			}
			else
			{
				sprites = new Sprite[0];
			}

			spriteNames = Array.ConvertAll(sprites, sprite => sprite.name.Replace(spriteNamesDisplayReplace, string.Empty));
			currentSpriteIndex = Mathf.Clamp(Array.IndexOf(spriteNames, spriteName), 0, spriteNames.Length);
			if (spriteNames.Length != 0)
			{
				spriteName = spriteNames[currentSpriteIndex];
				atlasImage.overrideSprite = sprites[currentSpriteIndex];
			}
			else
			{
				spriteName = string.Empty;
				atlasImage.overrideSprite = null;
			}
		}

		private void OnShowAtlasField()
		{
			EditorGUILayout.PropertyField(atlasSP, new GUIContent(fieldLabelSpirteAtlas));
		}

		private void OnShowSpriteNameField()
		{
			if (spriteNames != null && spriteNames.Length != 0)
			{
				currentSpriteIndex = EditorGUILayout.Popup(fieldLabelSpirte, currentSpriteIndex, spriteNames);
			}
		}

		private void DrawEditableField(Action onShowField, Action<bool> onValueChanged = null)
		{
			EditorGUI.BeginChangeCheck();
			onShowField?.Invoke();
			onValueChanged += isChanged =>
			{
				if (isChanged)
				{
					EditorUtility.SetDirty(target);
				}
			};
			onValueChanged?.Invoke(EditorGUI.EndChangeCheck());
		}

		private void OnReset()
		{
			atlasSP.objectReferenceValue = null;
			RefreshSpriteNames();
		}
	}
}
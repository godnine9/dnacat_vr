namespace Ninth.GamePlay.DNACAT
{
	public class InputManager : Singleton<InputManager>
	{
		public IInputHandler Handler
		{
			get;
			private set;
		}

		protected override void OnInitialized()
		{
			switch (InputConfig.Instance.Source)
			{
				case InputSource.Builtin:
				case InputSource.InputSystem:
					Handler = new BuiltinInput(InputLock);
					break;
				default:
					break;
			}
		}

		private bool InputLock() => UIHelper.CursorState;
	}
}

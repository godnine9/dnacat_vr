﻿namespace Ninth.GamePlay.DNACAT
{
	public abstract class PrefsValue<T>
	{
		public readonly string Key;

		public abstract T Value
		{
			get;
			set;
		}

		public PrefsValue(string key)
		{
			Key = key;
		}

		public abstract void Save();

		public void SetAndSave(T value)
		{
			Value = value;
			Save();
		}
	}
}

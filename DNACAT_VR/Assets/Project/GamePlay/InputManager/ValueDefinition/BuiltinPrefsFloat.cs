﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class BuiltinPrefsFloat : PrefsValue<float>
	{
		public BuiltinPrefsFloat(string key) : base(key)
		{
		}

		public override float Value
		{
			get => PlayerPrefs.GetFloat(Key);
			set => PlayerPrefs.SetFloat(Key, value);
		}

		public override void Save()
		{
			PlayerPrefs.Save();
		}
	}
}

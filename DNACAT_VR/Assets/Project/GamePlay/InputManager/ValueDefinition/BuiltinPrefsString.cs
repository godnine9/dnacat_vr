﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	internal class BuiltinPrefsString : PrefsValue<string>
	{
		public BuiltinPrefsString(string key) : base(key)
		{
		}

		public override string Value
		{
			get => PlayerPrefs.GetString(Key);
			set => PlayerPrefs.SetString(Key, value);
		}

		public override void Save()
		{
			PlayerPrefs.Save();
		}
	}
}

﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public interface IInputHandler
	{
		public bool Lock { get; }

		public float MouseSensitivity { get; set; }

		public float GetAxis(string axisName);

		public Vector3 Move();

		public bool Jump();

		public int Express();

		public bool Attack();

		public bool Throw();

		public bool PickUpItem();

		public bool OpenTransactionUI();

		public bool OpenBagUI();

		public bool OnEscape();
	}
}

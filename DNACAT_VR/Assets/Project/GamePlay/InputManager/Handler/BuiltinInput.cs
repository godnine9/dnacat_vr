﻿using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class BuiltinInput : IInputHandler
	{
		private const string _axisHorizontal = "Horizontal";
		private const string _axisVertical = "Vertical";
		private const string _btnJump = "Jump";

		private readonly Func<bool> _inputLock;

		public InputOptions InputOptions => Preferences.Instance.InputOptions;

		public bool Lock => _inputLock();

		public float GetAxis(string axisName) => Input.GetAxis(axisName);

		public float MouseSensitivity
		{
			get => InputOptions.MouseSensitivity.Value;
			set => InputOptions.MouseSensitivity.Value = value;
		}

		public BuiltinInput(Func<bool> inputLock)
		{
			_inputLock = inputLock;
		}

		public Vector3 Move()
		{
			if (!Lock)
			{
				float x = GetAxis(_axisHorizontal);
				float z = GetAxis(_axisVertical);
				return new Vector3(x, 0f, z);
			}
			return Vector3.zero;
		}

		public bool Attack() => Input.GetKey(KeyCode.F) && !Lock;

		public bool Throw() => Input.GetKey(KeyCode.Q) && !Lock;

		public bool Jump() => Input.GetButton(_btnJump) && !Lock;

		public bool PickUpItem() => Input.GetKeyUp(KeyCode.E);

		public int Express()
		{
			if (!Lock)
			{
				if (Input.GetKeyDown(KeyCode.F1))
				{
					return 1;
				}
				else if (Input.GetKeyDown(KeyCode.F2))
				{
					return 2;
				}
				else if (Input.GetKeyDown(KeyCode.F3))
				{
					return 3;
				}
				else if (Input.GetKeyDown(KeyCode.F4))
				{
					return 4;
				}
				else if (Input.GetKeyDown(KeyCode.F5))
				{
					return 5;
				}
			}
			return 0;
		}

		public bool OpenTransactionUI() => Input.GetKeyDown(KeyCode.E);

		public bool OpenBagUI() => Input.GetKeyDown(KeyCode.B);

		public bool OnEscape() => Input.GetKeyDown(KeyCode.Escape);
	}
}

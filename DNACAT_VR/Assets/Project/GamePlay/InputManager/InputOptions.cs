﻿namespace Ninth.GamePlay.DNACAT
{
	public class InputOptions
	{
		public PrefsValue<float> MouseSensitivity
		{
			get;
			private set;
		}

		public InputOptions()
		{
			MouseSensitivity = new BuiltinPrefsFloat($"[{nameof(InputOptions)}]{nameof(MouseSensitivity)}");
		}
	}
}

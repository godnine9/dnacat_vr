using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using System.Collections.Generic;

namespace Ninth.GamePlay.DNACAT
{
	using CameraView = CameraManager.View;

	public class GameManager : SceneSingleton<GameManager>
	{
		[SerializeField]
		private string _testController = "XBot";
		[SerializeField]
		private Transform _spawnPoint = default;
		[SerializeField]
		private Transform _mainCameraTrans = null;
		[SerializeField]
		private MaterialsConfig _catMaterials = default;

		private PhysicsScene _physicsScene;

		public IReadOnlyList<Material> CatMaterials => _catMaterials.Materials;

		private CameraManager CameraManager => CameraManager.Instance;

		public Controller LocalPlayer
		{
			get;
			private set;
		}

		public PlayerInfo LocalPlayerInfo
		{
			get;
			private set;
		}

		public void Reborn(Transform transform)
		{
			transform.SetPositionAndRotation(_spawnPoint.position, _spawnPoint.rotation);
		}

		protected override void Awake()
		{
			base.Awake();
		}

		protected void Start()
		{
			UIHelper.CursorState = false;
			_physicsScene = SceneManager.GetActiveScene().GetPhysicsScene();
			int matIndex = Random.Range(0, _catMaterials.Materials.Count);
			var instantiationData = new object[]
			{
				matIndex,
			};
			var controllerObj = PhotonNetwork.Instantiate(_testController, _spawnPoint.position, _spawnPoint.rotation, 0, instantiationData);
			if (controllerObj && controllerObj.TryGetComponent<Controller>(out var controller))
			{
				if (controller.IsMine)
				{
					AudioManager.PlayMusic("BGM", true);
					controller.SetCamera(_mainCameraTrans);

					var firstPerson = new CameraFollow(controller.Body[BodyParts.Head].transform, Constants.CullingMask_FirstPerson);
					CameraManager.AddFollow(CameraView.FirstPerson, firstPerson);

					var throwView = new CameraFollow(controller.ThrowView, Constants.CullingMask_ThirdPerson);
					CameraManager.AddFollow(CameraView.FirstPerson, throwView);

					var thirdPerson = new CameraFollow(controller.Body[BodyParts.Hip].transform, Constants.CullingMask_ThirdPerson);
					CameraManager.AddFollow(CameraView.ThirdPerson, thirdPerson);
					CameraManager.AddLookAt(CameraView.ThirdPerson, thirdPerson.Transform);

					CameraManager.Initialize();
					LocalPlayerInfo = new PlayerInfo();
					LocalPlayerInfo.OnLevelUp += controller.LevelUp;
					
					controller.SetRenderersLayer(Constants.LayerMask_Self);
					controller.OnKilledEnemy += LocalPlayerInfo.GetEXP;
					controller.OnGetItem += LocalPlayerInfo.GetItem;
					LocalPlayer = controller;
					
					DontDestroyOnLoad(controller);
				}
			}
			else
			{
				Debug.LogError($"Asset not found in resources folder:{_testController}");
			}
		}

		private void FixedUpdate()
		{
			if (_physicsScene.IsValid())
			{
				_physicsScene.Simulate(Time.fixedDeltaTime);
			}
		}

		private void OnDestroy()
		{
			if (LocalPlayerInfo != null)
			{
				LocalPlayerInfo.Dispose();
				PlayerPrefs.DeleteAll();
			}
		}
	}
}

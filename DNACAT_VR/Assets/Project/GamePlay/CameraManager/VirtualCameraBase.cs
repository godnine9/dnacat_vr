﻿using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Ninth.GamePlay.DNACAT
{
	public class VirtualCameraBase
	{
		protected readonly List<CameraFollow> _follows;
		protected readonly List<Transform> _lookAts;

		public VirtualCameraBase(CinemachineVirtualCameraBase vCam)
		{
			VirtualCamera = vCam;
			_follows = new List<CameraFollow>();
			_lookAts = new List<Transform>();
		}

		public bool Enabled
		{
			get => VirtualCamera ? VirtualCamera.enabled : false;
			set
			{
				if (VirtualCamera)
				{
					VirtualCamera.enabled = value;
				}
			}
		}

		public int CurrentFollowIndex
		{
			get;
			private set;
		}

		public int CurrentLookAtIndex
		{
			get;
			private set;
		}

		protected CinemachineVirtualCameraBase VirtualCamera
		{
			get;
			private set;
		}

		public Transform Follow
		{
			get => VirtualCamera.Follow;
			set
			{
				if (VirtualCamera)
				{
					VirtualCamera.Follow = value;
				}
			}
		}

		public Transform LookAt
		{
			get => VirtualCamera.LookAt;
			set
			{
				if (VirtualCamera)
				{
					VirtualCamera.LookAt = value;
				}
			}
		}

		public CameraFollow CurrentFollowInfo
		{
			get;
			private set;
		}

		public void AddFollow(CameraFollow follow)
		{
			_follows.Add(follow);
		}

		public void AddLookAt(Transform lookAt)
		{
			_lookAts.Add(lookAt);
		}

		public void SwitchFollow(int index)
		{
			if (index < 0 || index > _follows.Count - 1)
			{
				return;
			}
			CurrentFollowInfo = _follows[index];
			Follow = CurrentFollowInfo.Transform;
			CurrentFollowIndex = index;
		}

		public void SwitchLookAt(int index)
		{
			if (index < 0 || index > _lookAts.Count - 1)
			{
				return;
			}
			LookAt = _lookAts[index];
			CurrentLookAtIndex = index;
		}
	}
}

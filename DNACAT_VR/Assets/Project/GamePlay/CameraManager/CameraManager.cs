using System;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace Ninth.GamePlay.DNACAT
{
	public class CameraManager : SceneSingleton<CameraManager>
	{
		public enum View
		{
			FirstPerson = 10,
			ThirdPerson = 11,
		}

		public event Action<View> OnVisionChanged;

		[SerializeField]
		private Camera _mainCamera = default;
		[SerializeField]
		private CinemachineBrain _brain = default;
		[SerializeField]
		private CinemachineVirtualCameraBase _firstPersonCam = default;
		[SerializeField]
		private CinemachineVirtualCameraBase _thirdPersonCam = default;
		[SerializeField]
		private View _startupView = default;

		private IReadOnlyDictionary<View, VirtualCameraBase> _virtualCameras;

		public CinemachineBrain Brain => _brain;

		public Transform MainCamera => _brain.transform;

		public View StartupView => _startupView;

		private IInputHandler InputHandler => InputManager.Instance.Handler;

		public View CurrentView
		{
			get;
			private set;
		}

		public VirtualCameraBase Current
		{
			get;
			private set;
		}

		protected override void Awake()
		{
			base.Awake();
			CinemachineCore.GetInputAxis = GetAxis;
			_virtualCameras = new Dictionary<View, VirtualCameraBase>
			{
				{ View.FirstPerson, new VirtualCameraBase(_firstPersonCam) },
				{ View.ThirdPerson, new VirtualCameraBase(_thirdPersonCam) },
			};
		}

		private void Start()
		{
			ResetCamera();
		}

		private void OnDestroy()
		{
			CinemachineCore.GetInputAxis = Input.GetAxis;
		}

		public void AddFollow(View view, CameraFollow follow)
		{
			if (_virtualCameras.TryGetValue(view, out var virtualCamera))
			{
				virtualCamera.AddFollow(follow);
			}
		}

		public void AddLookAt(View view, Transform target)
		{
			if (_virtualCameras.TryGetValue(view, out var virtualCamera))
			{
				virtualCamera.AddLookAt(target);
			}
		}

		public void Initialize()
		{
			foreach (var virtualCamera in _virtualCameras)
			{
				virtualCamera.Value.SwitchFollow(0);
				virtualCamera.Value.SwitchLookAt(0);
			}
		}

		public void SwitchCamera(View target)
		{
			if (CurrentView == target)
			{
				return;
			}

			CurrentView = target;
			foreach (var pair in _virtualCameras)
			{
				var vCam = pair.Value;
				vCam.Enabled = pair.Key == target;
				if (vCam.Enabled)
				{
					Current = pair.Value;
				}
			}

			if (_mainCamera)
			{
				_mainCamera.cullingMask = Current.CurrentFollowInfo.CullingMask;
			}
			OnVisionChanged?.Invoke(target);
		}

		public void ResetCamera()
		{
			SwitchCamera(_startupView);
		}

		public void SwitchFollow(View view, int index)
		{
			if (Current.CurrentFollowIndex == index)
			{
				return;
			}
			if (!_mainCamera)
			{
				return;
			}
			if (_virtualCameras.TryGetValue(view, out var virtualCamera))
			{
				virtualCamera.SwitchFollow(index);
				_mainCamera.cullingMask = virtualCamera.CurrentFollowInfo.CullingMask;
			}
		}

		public void SwitchLookAt(View view, int index)
		{
			if (Current.CurrentLookAtIndex == index)
			{
				return;
			}
			if (!_mainCamera)
			{
				return;
			}
			if (_virtualCameras.TryGetValue(view, out var virtualCamera))
			{
				virtualCamera.SwitchLookAt(index);
				_mainCamera.cullingMask = virtualCamera.CurrentFollowInfo.CullingMask;
			}
		}

		private float GetAxis(string axisName) => !UIHelper.CursorState ? InputHandler.GetAxis(axisName) : 0f;
	}
}

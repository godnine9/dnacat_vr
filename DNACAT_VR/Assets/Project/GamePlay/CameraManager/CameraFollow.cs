﻿using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class CameraFollow
	{
		public readonly Transform Transform;
		public readonly int CullingMask;

		public CameraFollow(Transform transform, int cullingMask)
		{
			Transform = transform;
			CullingMask = cullingMask;
		}
	}
}

﻿using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class VFXManager : SceneSingleton<VFXManager>
	{
		[SerializeField]
		private List<GameObject> _effects = default;

		private Dictionary<string, GameObject> _effectTable;

		protected override void Awake()
		{
			base.Awake();
			_effectTable = new Dictionary<string, GameObject>();
			foreach (var effect in _effects)
			{
				if (_effectTable.ContainsKey(effect.name))
				{
					Debug.LogError($"Duplicate effect prefab name dectected:{effect.name}");
					continue;
				}
				_effectTable.Add(effect.name, effect);
			}
		}

		public GameObject SpawnEffect(string prefabName, Vector3 position, Quaternion rotation)
		{
			if (_effectTable.TryGetValue(prefabName, out var effect))
			{
				return Instantiate(effect, position, rotation);
			}
			Debug.LogError($"Effect prefab not found:{prefabName}");
			return null;
		}
	}
}

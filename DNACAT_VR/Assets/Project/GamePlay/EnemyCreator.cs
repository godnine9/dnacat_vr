using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Ninth.GamePlay.DNACAT
{
	public class EnemyCreator : MonoBehaviourPunCallbacks
	{
		[Serializable]
		private class Area
		{
			[SerializeField]
			private PhotonView _center = default;
			[SerializeField]
			private float _spawnRadius = 10f;
			[SerializeField]
			private int _maxCount = 30;
			[SerializeField]
			private int _enemyID = default;

			private Dictionary<int, Enemy> _enemies;
			private string _prefabName;
			private Coroutine _spawnProcess;

			public int ViewID => _center.ViewID;

			public void Initialize()
			{
				_enemies = new Dictionary<int, Enemy>();
				if (EnemyDataSet.Instance.TryGetData(_enemyID, out var data))
				{
					_prefabName = data.Prefab;
					return;
				}
				Debug.LogError($"Enemy ID not found:{_enemyID}");
			}

			public void StartSpawnProcess()
			{
				_spawnProcess = _center.StartCoroutine(SpawnProcess());
			}

			public bool Contains(Enemy enemy)
			{
				return _enemies.ContainsKey(enemy.View.ViewID);
			}

			public bool TryDespawn(Enemy enemy)
			{
				return Contains(enemy) && _enemies.Remove(enemy.View.ViewID);
			}

			public bool Spawn()
			{
				if (_enemies.Count < _maxCount)
				{
					var instantiationData = new object[]
					{
						_enemyID,
						ViewID,
					};

					var instance = PhotonNetwork.InstantiateRoomObject(_prefabName, GetRandomPosition(), Quaternion.identity, 0, instantiationData);
					if (instance.TryGetComponent<Enemy>(out var enemy))
					{
						_enemies.Add(enemy.View.ViewID, enemy);
						return true;
					}
					else
					{
						Debug.LogError("Enemy component not found.");
						PhotonNetwork.Destroy(instance);
					}
				}
				return false;
			}

			public void AddEnemy(Enemy enemy)
			{
				_enemies.Add(enemy.View.ViewID, enemy);
			}

			public void DrawGizmos()
			{
				Gizmos.color = Color.red;
				Gizmos.DrawWireSphere(_center.transform.position, _spawnRadius);
			}

			private IEnumerator SpawnProcess()
			{
				while (true)
				{
					float delay = UnityEngine.Random.Range(_minSpawnTime, _maxSpawnTime);
					yield return new WaitForSeconds(delay);
					Spawn();
				}
			}

			private Vector3 GetRandomPosition()
			{
				Vector2 randomCircle = UnityEngine.Random.insideUnitCircle * _spawnRadius;
				return _center.transform.position + new Vector3(randomCircle.x, 0, randomCircle.y);
			}
		}

		private const float _minSpawnTime = 1f;
		private const float _maxSpawnTime = 3f;

		[SerializeField]
		private List<Area> _areas = default;

		private Dictionary<int, Area> _areasTable;

		public static EnemyCreator Instance
		{
			get;
			private set;
		}

		public void Awake()
		{
			Instance = this;

			_areasTable = new Dictionary<int, Area>();
			bool isMasterClient = PhotonNetwork.IsMasterClient;
			foreach (var area in _areas)
			{
				_areasTable.Add(area.ViewID, area);
				area.Initialize();
				if (isMasterClient)
				{
					area.StartSpawnProcess();
				}
			}
		}

		public void Despawn(Enemy enemy)
		{
			if (_areasTable.TryGetValue(enemy.AreaViewID, out var area))
			{
				if (area.TryDespawn(enemy))
				{
					enemy.DropItems();
					PhotonNetwork.Destroy(enemy.View);
				}
			}
		}

		public override void OnMasterClientSwitched(Player newMasterClient)
		{
			if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
			{
				var enemise = FindObjectsOfType<Enemy>();
				foreach (var enemy in enemise)
				{
					if (_areasTable.TryGetValue(enemy.AreaViewID, out var area))
					{
						area.AddEnemy(enemy);
					}
				}

				foreach (var area in _areas)
				{
					area.StartSpawnProcess();
				}
			}
		}

		private void OnDrawGizmos()
		{
			foreach (var area in _areas)
			{
				area.DrawGizmos();
			}
		}
	}
}

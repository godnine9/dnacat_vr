﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
    public class AudioManager : Singleton<AudioManager>
    {
        public static GameObject SoundPlayObject;
        public static Dictionary<string, AudioSource> Musics = null;//音樂表
        public static Dictionary<string, AudioSource> Sounds = null;//音效表

        private static float BGMVolumValue;
        private static float SoundVolumVaue;
        protected override void OnInitialized()
        {
            SoundPlayObject = new GameObject("SoundPlayObject");
            SoundPlayObject.AddComponent<SoundScan>();
            DontDestroyOnLoad(SoundPlayObject);
            Musics = new Dictionary<string, AudioSource>();
            Sounds = new Dictionary<string, AudioSource>();
            BGMVolumValue = 1;
            SoundVolumVaue = 1;
        }


        public static AudioSource GetPlayingMusicAudioSource(string url)
        {
            AudioSource aAudioSource = null;
            if (Musics.ContainsKey(url))
            {
                aAudioSource = Musics[url];
            }
            return aAudioSource;
        }

        public static IEnumerator FadeOutSnd(AudioSource audioSource, float FadeTime)
        {
            float startVolume = audioSource.volume;

            while (audioSource.volume > 0)
            {
                audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

                yield return null;
            }

            audioSource.Stop();
            audioSource.volume = startVolume;
        }
        public static IEnumerator MusicFadeOut(string url, float fadeTime)
        {
            AudioSource audioSource = GetPlayingMusicAudioSource(url);
            if (audioSource != null)
            {
                float startVolume = audioSource.volume;

                while (audioSource.volume > 0)
                {
                    audioSource.volume -= startVolume * Time.deltaTime / fadeTime;
                    yield return null;
                }
                audioSource.Stop();
                audioSource.volume = startVolume;
            }
        }

        public static IEnumerator MusicFadeIn(string url, float volume, float fadeTime, bool is_loop = true)
        {
            var audioSource = PlayMusic(url, is_loop);
            audioSource.volume = 0f;
            if (audioSource != null)
            {
                while (audioSource.volume < volume)
                {
                    audioSource.volume += volume * Time.deltaTime / fadeTime;
                    yield return null;
                }
            }
            audioSource.volume = volume;
        }

        public static AudioSource PlayMusic(string iFile, bool is_loop = true, float delaytime = 0f)
        {
            StopAllMusic();
            string aPath = "Audio/Music/" + iFile;
            if (Musics == null)
            {
                Debug.Log("SndManager musics 未初始");
                return null;
            }
            AudioSource aAudioSource = null;
            if (Musics.ContainsKey(aPath))
            {
                aAudioSource = Musics[aPath];
            }
            else
            {
                GameObject s = new GameObject(aPath);
                s.transform.parent = SoundPlayObject.transform;

                aAudioSource = s.AddComponent<AudioSource>();
                AudioClip clip = Resources.Load<AudioClip>(aPath);
                aAudioSource.clip = clip;
                aAudioSource.loop = is_loop;
                aAudioSource.playOnAwake = true;
                aAudioSource.spatialBlend = 0.0f;
                Musics.Add(aPath, aAudioSource);
            }

            aAudioSource.volume = BGMVolumValue;
            aAudioSource.loop = is_loop;
            aAudioSource.enabled = true;
            aAudioSource.PlayDelayed(delaytime);
            aAudioSource.Play();

            return aAudioSource;
        }

        public static void stop_music(string url)
        {
            AudioSource aAudioSource = null;
            if (!Musics.ContainsKey(url))
            {
                return;
            }
            aAudioSource = Musics[url];
            aAudioSource.Stop();
        }

        public static void StopAllMusic()
        {
            foreach (AudioSource aAudioSource in Musics.Values)
            {
                aAudioSource.Stop();
            }
        }

        public static void ClearMusic(string url)
        {
            AudioSource aAudioSource = null;
            if (!Musics.ContainsKey(url))
            {
                return;
            }
            aAudioSource = Musics[url];
            Musics[url] = null;
            GameObject.Destroy(aAudioSource.gameObject);
        }

        public static void Clear()
        {
            foreach (AudioSource s in Musics.Values)
            {
                GameObject.Destroy(s.gameObject);
            }
            Musics.Clear();

            foreach (AudioSource s in Sounds.Values)
            {
                GameObject.Destroy(s.gameObject);
            }
            Sounds.Clear();
        }

        public static void PlaySound(string iFileName, bool is_loop = false, float delaytime = 0.0f)
        {
            string url = string.Empty;
            url = "Audio/Sound/" + iFileName;
            if (string.IsNullOrEmpty(url)) { return; }
            if (Sounds == null)
            {
                Debug.Log("SndManager play_effect effects 未初始");
                return;
            }
            AudioSource aAudioSource = null;
            if (Sounds.ContainsKey(url))
            {
                aAudioSource = Sounds[url];
            }
            else
            {
                GameObject s = new GameObject(url);
                s.transform.parent = SoundPlayObject.transform;

                aAudioSource = s.AddComponent<AudioSource>();
                AudioClip clip = Resources.Load<AudioClip>(url);
                aAudioSource.clip = clip;
                aAudioSource.playOnAwake = true;
                aAudioSource.spatialBlend = 0.0f;
                Sounds.Add(url, aAudioSource);
            }
            aAudioSource.volume = SoundVolumVaue;
            aAudioSource.loop = is_loop;
            aAudioSource.enabled = true;
            aAudioSource.PlayDelayed(delaytime);
            aAudioSource.Play();
        }
        public static void StopSound(string url)
        {
            AudioSource aAudioSource = null;
            if (!Sounds.ContainsKey(url))
            {
                return;
            }
            aAudioSource = Sounds[url];
            aAudioSource.Stop();
        }

        public static void StopAllSound()
        {
            foreach (AudioSource aAudioSource in Sounds.Values)
            {
                aAudioSource.Stop();
            }
        }

        public static void ClearSound(string url)
        {
            AudioSource aAudioSource = null;
            if (!Sounds.ContainsKey(url))
            {
                return;
            }
            aAudioSource = Sounds[url];
            Sounds[url] = null;
            GameObject.Destroy(aAudioSource.gameObject);
        }

        public static void StopAllAudio()
        {
            foreach (AudioSource aAudioSource in Musics.Values)
            {
                if (!aAudioSource.isPlaying)
                {
                    aAudioSource.enabled = false;
                }
            }

            foreach (AudioSource aAudioSource in Sounds.Values)
            {
                if (!aAudioSource.isPlaying)
                {
                    aAudioSource.enabled = false;
                }
            }
        }

        public static void SetAllSoundsVolume(float iVolume)
        {
            SoundVolumVaue = iVolume;
            foreach (AudioSource aAudioSource in Sounds.Values)
            {
                aAudioSource.volume = SoundVolumVaue;
            }
        }

        public static void SetAllMusicVolume(float iVolume)
        {
            BGMVolumValue = iVolume;
            foreach (AudioSource aAudioSource in Musics.Values)
            {
                aAudioSource.volume = BGMVolumValue;
            }
        }

        public static float GetSoundsVolume()
        {
            foreach (AudioSource aAudioSource in Sounds.Values)
            {
                aAudioSource.volume = SoundVolumVaue;
            }
            return SoundVolumVaue;
        }

        public static float GetMusicsVolume()
        {
            foreach (AudioSource aAudioSource in Musics.Values)
            {
                aAudioSource.volume = BGMVolumValue;
            }
            return BGMVolumValue;
        }
    }
}
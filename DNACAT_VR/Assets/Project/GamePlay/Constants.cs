using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public static class Constants
	{
		public const float Gravity = -9.81f;
		public const float GroundedAccelerate = -2f;

		public const string Tag_Interaction = "Interaction";
		public const string Tag_Enemy = "Enemy";
		public const string Tag_Environment = "Environment";

		public const string AnimNameGrounded = "Grounded";
		public const string AnimNameHighFall = "HighFall";
		public const string AnimNameMoveX = "MoveX";
		public const string AnimNameMoveY = "MoveY";
		public const string AnimNameExpress = "Express";
		public const string AnimNameExpression = "Expression";
		public const string AnimNameAttack = "Attack";
		public const string AnimNameThrow = "Throw";
		public const string AnimNameLevelUp = "LevelUp";
		public const string AnimNameDead = "Dead";

		public const string AnimStateNeutral = "Neutral";
		public const string AnimStateAttack01 = "Attack 01";
		public const string AnimStateAttack02 = "Attack 02";
		public const string AnimStateAttack03 = "Attack 03";
		public const string AnimStateThrowReady = "Throw Ready";
		public const string AnimStateThrow = "Throw";
		public const string AnimStateJump = "Jump";
		public const string AnimStateDance = "Dance";
		public const string AnimStateHighFall = "High Fall";
		public const string AnimStateDead = "Dead";

		public static readonly int LayerMask_Environment = LayerMask.NameToLayer("Environment");
		public static readonly int LayerMask_Enemy = LayerMask.GetMask("Enemy");
		public static readonly int LayerMask_Player = LayerMask.GetMask("Player");
		public static readonly int LayerMask_Self = LayerMask.NameToLayer("Self");
		public static readonly int CullingMask_FirstPerson = ~LayerMask.GetMask("UI", "Self");
		public static readonly int CullingMask_ThirdPerson = ~LayerMask.GetMask("UI");
	}
}

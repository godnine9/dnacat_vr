using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	[Serializable]
	public class PhysicSimulation
	{
		[SerializeField]
		private LineRenderer _line = default;
		[SerializeField]
		private int _maxFrames = 100;

		public int MaxFrames => _maxFrames;

		public bool Visible
		{
			get => _line ? _line.enabled : false;
			set
			{
				if (_line)
				{
					_line.enabled = value;
				}
			}
		}

		public void ResetLineCount()
		{
			_line.positionCount = 0;
			_line.positionCount = _maxFrames;
		}

		public void SetLinePosition(int index, Vector3 position) => _line.SetPosition(index, position);
	}
}

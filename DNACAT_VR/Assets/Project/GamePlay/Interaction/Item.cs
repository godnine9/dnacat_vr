using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	[RequireComponent(typeof(PhotonView))]
	public class Item : InteractionBase, IPickable
	{
		[SerializeField]
		private Transform _canvas = null;
		[SerializeField]
		private ItemType _type = default;
		[SerializeField]
		private PhotonView _photonView = null;
		[SerializeField]
		private Collider _collider = null;
		[SerializeField]
		private int _amount = 1;

		public ItemData Data
		{
			get;
			private set;
		}

		public override Collider Collider => _collider;

		public override TriggerFlags TriggerFlags => TriggerFlags.Item;

		public ItemType Type => _type;

		public int Amount => _amount;

		private Transform MainCamera => CameraManager.Instance.MainCamera;

		public bool CanvasEnabled
		{
			get => _canvas.gameObject.activeSelf;
			private set => _canvas.gameObject.SetActive(value);
		}

		private void Start()
		{
			if (!_photonView)
			{
				_photonView = GetComponent<PhotonView>();
			}
			CanvasEnabled = false;
		}

		private void LateUpdate()
		{
			if (MainCamera)
			{
				_canvas.rotation = Quaternion.LookRotation(_canvas.position - MainCamera.position);
			}
		}

		public override void OnEntered(int viewID)
		{
			CanvasEnabled = true;
		}

		public override void OnStayed(int viewID)
		{
		}

		public override void OnExited(int viewID)
		{
			CanvasEnabled = false;
		}

		public void OnPicked()
		{
			switch (_type)
			{
				case ItemType.Coin:
					AudioManager.PlaySound("GetMoney");
					break;
				case ItemType.Item:
					AudioManager.PlaySound("GetItem");
					break;
			}

			_photonView.RpcSecure(nameof(RPC_OnPickedUp), RpcTarget.All, true);
		}

		[PunRPC]
		private void RPC_OnPickedUp()
		{
			if (_photonView.IsMine)
			{
				PhotonNetwork.Destroy(_photonView.gameObject);
			}
		}
	}

	public enum ItemType : int
	{
		Coin = 10500001,
		Item = 10500002,
	}
}

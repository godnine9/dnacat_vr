﻿using System;

namespace Ninth.GamePlay.DNACAT
{
	[Flags]
	public enum TriggerFlags
	{
		None = 0,
		Transaction = 1,		// 交易 NPC
		WeaponProvider = 2,     // 武器提供區
		ProjectileProvider = 4, // 投射物提供區
		Item = 8,				// 鄰近道具
	}
}

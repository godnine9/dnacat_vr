namespace Ninth.GamePlay.DNACAT
{
	public interface IPickable
	{
		public ItemType Type { get; }

		public int Amount { get; }

		public void OnPicked();
	}
}

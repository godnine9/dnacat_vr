using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class NPC : InteractionBase
	{
		[SerializeField]
		private Transform _canvas = null;
		[SerializeField]
		private Collider _collider = null;

		public override Collider Collider => _collider;

		public override TriggerFlags TriggerFlags => TriggerFlags.Transaction;

		private Transform MainCamera => CameraManager.Instance.MainCamera;

		public bool CanvasEnabled
		{
			get => _canvas.gameObject.activeSelf;
			private set => _canvas.gameObject.SetActive(value);
		}

		private void Start()
		{
			CanvasEnabled = false;
		}

		private void LateUpdate()
		{
			if (MainCamera)
			{
				_canvas.rotation = Quaternion.LookRotation(_canvas.position - MainCamera.position);
			}
		}

		public override void OnEntered(int viewID) => CanvasEnabled = true;

		public override void OnExited(int viewID) => CanvasEnabled = false;
	}
}

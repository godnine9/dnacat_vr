﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public abstract class InteractionBase : MonoBehaviour, ITriggerFlagsProvider
	{
		public abstract Collider Collider { get; }

		public abstract TriggerFlags TriggerFlags { get; }

		public virtual void OnEntered(int viewID)
		{
		}

		public virtual void OnStayed(int viewID)
		{
		}

		public virtual void OnExited(int viewID)
		{
		}
	}
}
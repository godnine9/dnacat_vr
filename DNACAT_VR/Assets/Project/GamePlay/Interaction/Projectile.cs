﻿using System;
using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	[RequireComponent(typeof(PhotonView), typeof(Rigidbody))]
	public class Projectile : MonoBehaviour
	{
		public event Action<Enemy> OnHitEnemy;

		[SerializeField]
		private PhotonView _photonView = default;
		[SerializeField]
		private Rigidbody _rigidbody = default;
		[SerializeField]
		private float _lifeTime = 5f;

		private float _lifeTimeCount;
		private bool IsThrowed;
		public Rigidbody Rigidbody => _rigidbody;

		private void Awake()
		{
			if (!_photonView)
			{
				_photonView = GetComponent<PhotonView>();
			}

			if (!_rigidbody)
			{
				_rigidbody = GetComponent<Rigidbody>();
			}
			IsThrowed = false;
			_lifeTimeCount = _lifeTime;
		}

		private void Update()
		{
			if (!_photonView.IsMine)
			{
				return;
			}

			_lifeTimeCount -= Time.deltaTime;
			if (_lifeTimeCount < 0f)
			{
				PhotonNetwork.Destroy(_photonView);
			}
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (!_photonView.IsMine)
			{
				return;
			}

			var hitTarget = collision.gameObject;
			if (hitTarget.CompareTag(Constants.Tag_Enemy))
			{
				if (hitTarget.TryGetComponent<Enemy>(out var enemy) && !IsThrowed)
				{
					OnHitEnemy?.Invoke(enemy);
					PhotonNetwork.Destroy(_photonView);
				}
			}
			else if (collision.gameObject.CompareTag(Constants.Tag_Environment))
			{
				IsThrowed = true;
			}
		}

		public void Launch(Vector3 direction, float force)
		{
			_rigidbody.AddForce(direction * force);
			_rigidbody.AddTorque(Vector3.back, ForceMode.Impulse);
		}
	}
}

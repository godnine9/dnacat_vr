﻿namespace Ninth.GamePlay.DNACAT
{
	public interface ITriggerFlagsProvider
	{
		public TriggerFlags TriggerFlags { get; }

		public void OnEntered(int viewID);

		public void OnExited(int viewID);
	}
}

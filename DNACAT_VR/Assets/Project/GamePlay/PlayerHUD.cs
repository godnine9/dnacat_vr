using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Ninth.GamePlay.DNACAT
{
	public class PlayerHUD : MonoBehaviour
	{
		[SerializeField]
		private Canvas _canvas = null;
		[SerializeField]
		private Text _name = null;
		[SerializeField]
		private Text _message = null;
		[SerializeField]
		private Image _messageBase = null;
		//[SerializeField]
		//private Image _specker = null;
		//[SerializeField]
		//private Image _recorder = null;
		[SerializeField]
		private Sprite _localMessageSprite = null;
		[SerializeField]
		private Sprite _messageSprite = null;

		private static readonly YieldInstruction _delayShowChatBubble = new WaitForSeconds(3f);
		private Transform _mainCamera;
		private Coroutine _processChatBubble;

		public void Initialize(string nickName, bool isLocalPlayer)
		{
			_canvas.gameObject.SetActive(true);
			_name.gameObject.SetActive(true);
			_name.text = nickName;
			_message.gameObject.SetActive(false);
			_messageBase.sprite = isLocalPlayer ? _localMessageSprite : _messageSprite;
			_messageBase.gameObject.SetActive(false);
		}

		private void Awake()
		{
			if (!_mainCamera && Camera.main)
			{
				_mainCamera = Camera.main.transform;
			}
		}

		private void LateUpdate()
		{
			if (_mainCamera)
			{
				transform.rotation = Quaternion.LookRotation(transform.position - _mainCamera.position);
			}
		}

		public void ShowChatBubble(string msg)
		{
			_message.text = msg;
			if (_processChatBubble != null)
			{
				StopCoroutine(_processChatBubble);
			}
			_processChatBubble = StartCoroutine(ShowChatBubbleProcess());
		}

		private IEnumerator ShowChatBubbleProcess()
		{
			_messageBase.gameObject.SetActive(true);
			_message.gameObject.SetActive(true);
			yield return _delayShowChatBubble;
			_messageBase.gameObject.SetActive(false);
			_message.gameObject.SetActive(false);
			_processChatBubble = null;
		}
	}
}

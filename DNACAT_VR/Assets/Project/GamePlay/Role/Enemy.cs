using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using Photon.Pun;
using System;

namespace Ninth.GamePlay.DNACAT
{
	[RequireComponent(typeof(PhotonView), typeof(NavMeshAgent), typeof(Animator))]
	public class Enemy : HitableObject
	{
		private enum State
		{
			Idle,
			Move,
			Chase,
			Attack,
			Hurt,
			Dead,
		}

		[Serializable]
		private struct HitBox
		{
			public Vector3 Center;
			public Vector3 Size;
		}

		private const float _randomMoveRange = 2f;

		[SerializeField]
		private PhotonView _photonView = default;
		[SerializeField]
		private Canvas _canvas = default;
		[SerializeField]
		private NavMeshAgent _agent = default;
		[SerializeField]
		private Animator _animator = default;
		[SerializeField]
		private Image _hpSlider = default;
		[SerializeField]
		private BoxCollider _hitBox = default;

		private State _currentState;
		private float _updateFreq;
		private Transform _attackTarget;
		private AttackEvents[] _attackEvents;

		private int _hp;
		private int _atk;

		public EnemyData Data
		{
			get;
			private set;
		}

		public int AreaViewID
		{
			get;
			private set;
		}

		public PhotonView View => _photonView;

		private Transform MainCamera => CameraManager.Instance.MainCamera;

		private void Awake()
		{
			_photonView = GetComponent<PhotonView>();
			_hpSlider.fillAmount = 1f;
			int id = (int)_photonView.InstantiationData[0];
			AreaViewID = (int)_photonView.InstantiationData[1];

			_attackEvents = _animator.GetBehaviours<AttackEvents>();
			foreach (var attackEvent in _attackEvents)
			{
				attackEvent.Initialize(PhotonNetwork.IsMasterClient);
				attackEvent.OnAttack += Attack;
				attackEvent.OnBeat += OnBeatPlayer;
			}

			if (EnemyDataSet.Instance.TryGetData(id, out var data))
			{
				Data = data;
				_hp = data.HP;
				_atk = data.Attack;
			}

			ChangeState(State.Idle);
		}

		public void Update()
		{
			StateUpdate();
		}

		private void LateUpdate()
		{
			if (MainCamera)
			{
				_canvas.transform.rotation = Quaternion.LookRotation(_canvas.transform.position - MainCamera.position);
			}
		}

		private void OnDestroy()
		{
			foreach (var attackEvent in _attackEvents)
			{
				attackEvent.OnAttack -= Attack;
				attackEvent.OnBeat -= OnBeatPlayer;
			}
		}

		private void OnDrawGizmos()
		{
			Gizmos.color = Color.red;
			Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
			Gizmos.DrawWireCube(_hitBox.center, _hitBox.size);
		}

		private void StateUpdate()
		{
			_updateFreq += Time.deltaTime;
			switch (_currentState)
			{
				case State.Idle:
					if(_updateFreq > 3f)
					{
						ChangeState(State.Move);
					}
					break;
				case State.Move:
					if (_updateFreq > 1f)
					{
						ChangeState(State.Idle);
					}
					break;
				case State.Hurt:
					if (_updateFreq > 0.3f)
					{
						ChangeState(State.Chase);
					}
					break;
				case State.Dead:
					if (_updateFreq > 2f)
					{
						_photonView.RpcSecure(nameof(RPC_OnDead), RpcTarget.All, true);
					}
					break;
				case State.Chase:
					if (!_attackTarget)
					{
						ChangeState(State.Idle);
						return;
					}
					_agent.SetDestination(_attackTarget.position);
					Vector3 direction = _attackTarget.position - transform.position;
					direction.y = 0f;
					transform.forward = direction;
					if (_updateFreq > 10f)
					{
						_agent.SetDestination(transform.position);
						ChangeState(State.Idle);
					}
					else if (Vector3.Distance(transform.position, _attackTarget.position) < 1.5f)
					{
						ChangeState(State.Attack);
						_agent.SetDestination(transform.position);
					}
					break;
				case State.Attack:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Attack");
					if (_updateFreq > 1.5f)
					{
						ChangeState(State.Chase);
					}
					break;
			}
		}

		private void ChangeState(State state)
		{
			_updateFreq = 0;
			_currentState = state;
			switch (state)
			{
				case State.Idle:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Idle");
					break;
				case State.Move:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Move");
					Vector2 random = UnityEngine.Random.insideUnitCircle * _randomMoveRange;
					Vector3 dir = new Vector3(random.x, 0f, random.y);
					Vector3 destination = transform.position + dir;
					transform.forward = dir;
					_agent.SetDestination(destination);
					break;
				case State.Hurt:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Hurt");
					break;
				case State.Dead:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Die");
					break;
				case State.Chase:
					_photonView.RpcSecure(nameof(PlayMotion), RpcTarget.All, true, "Move");
					break;
			}
		}

		public override bool OnHit(int atk, int sourceViewID)
		{
			if (!_isHitable)
			{
				return false;
			}

			if (_currentState == State.Dead)
			{
				return false;
			}
			bool isDead = _hp - atk <= 0;
			ChangeState(State.Hurt);
			_photonView.RpcSecure(nameof(RPC_OnHit), RpcTarget.All, true, atk, sourceViewID);
			return isDead;
		}

		public void DropItems()
		{
			for (int i = 0; i < Data.Item.Length; i++)
			{
				int itemID = Data.Item[i];
				int itemCount = Data.ItemCount[i];
				for (int j = 0; j < itemCount; j++)
				{
					if (ItemDataSet.Instance.TryGetData(itemID, out var itemData))
					{
						var random = UnityEngine.Random.insideUnitCircle * 2;
						var position = transform.position + new Vector3(random.x, 0, random.y);
						PhotonNetwork.InstantiateRoomObject(itemData.Prefab, position, Quaternion.identity);
					}
				}
			}
		}

		private Collider[] Attack()
		{
			if (PhotonNetwork.IsMasterClient)
			{
				Vector3 center = _hitBox.transform.position;
				Vector3 size = _hitBox.size;
				Quaternion orientation = _hitBox.transform.rotation;
				return Physics.OverlapBox(center, size * 0.5f, orientation, Constants.LayerMask_Player, QueryTriggerInteraction.Collide);
			}
			return null;
		}

		private void OnBeatPlayer(HitableObject player)
		{
			if(!PhotonNetwork.IsMasterClient)
            {
				return;
            }
			_photonView.RpcSecure(nameof(RPC_OnBeatPlayer), RpcTarget.All, true, player.photonView.ViewID);
		}

		[PunRPC]
		private void RPC_OnBeatPlayer(int playerInt)
		{
			if (GameManager.Instance.LocalPlayer.OnHit(_atk, playerInt))
			{
				_attackTarget = null;
				_agent.SetDestination(transform.position);
				ChangeState(State.Idle);
			}
		}
		[PunRPC]
		private void PlayMotion(string motion)
		{
			_animator.Play(motion);
		}

		[PunRPC]
		private void RPC_OnHit(int atk, int sourceViewID)
		{
			AudioManager.PlaySound("HitEnemy");
			Vector3 position = transform.position + transform.up * .5f;
			VFXManager.Instance.SpawnEffect("VFX_Hit", position, Quaternion.identity);

			_hp -= atk;
			if(_hp <= 0)
			{
				_hp = 0;
				_hpSlider.fillAmount = 0f;
				ChangeState(State.Dead);
			}
			else
			{
				_hpSlider.fillAmount = _hp * 0.1f;
				ChangeState(State.Hurt);
				_attackTarget = PhotonNetwork.GetPhotonView(sourceViewID).transform;
			}
		}

		[PunRPC]
		private void RPC_OnDead()
		{
			EnemyCreator.Instance.Despawn(this);
		}
	}
}

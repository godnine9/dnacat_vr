﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class AnimatorHandler
	{
		[Flags]
		public enum State
		{
			Neutral = 0,
			Move = 1,
			Dance = 2,
			Jump = 4,
			Attack = 8,
			Throw = 16,
			HighFall = 32,
			Dead = 64,
		}

		private readonly Animator _animator;
		private readonly List<AttackEvents> _attackEventKeys;
		private readonly List<ThrowEvents> _throwEventKeys;
		private readonly List<ViewEvents> _highFallEventKeys;
		private readonly List<LevelUpEvents> _levelUpEventKeys;

		public bool IsGrounded
		{
			get => _animator.GetBool(Constants.AnimNameGrounded);
			set => _animator.SetBool(Constants.AnimNameGrounded, value);
		}

		public bool HighFall
		{
			get => _animator.GetBool(Constants.AnimNameHighFall);
			set => _animator.SetBool(Constants.AnimNameHighFall, value);
		}

		public float MoveX
		{
			get => _animator.GetFloat(Constants.AnimNameMoveX);
			set => _animator.SetFloat(Constants.AnimNameMoveX, value);
		}

		public float MoveY
		{
			get => _animator.GetFloat(Constants.AnimNameMoveY);
			set => _animator.SetFloat(Constants.AnimNameMoveY, value);
		}

		public Vector3 Move
		{
			get => new Vector3(MoveX, 0f, MoveY);
			set
			{
				MoveX = value.x;
				MoveY = value.z;
			}
		}

		public int Expression
		{
			get => (int)_animator.GetFloat(Constants.AnimNameExpression);
			set => _animator.SetFloat(Constants.AnimNameExpression, value);
		}

		public bool Attack
		{
			get => _animator.GetBool(Constants.AnimNameAttack);
			set => _animator.SetBool(Constants.AnimNameAttack, value);
		}

		public bool Throw
		{
			get => _animator.GetBool(Constants.AnimNameThrow);
			set => _animator.SetBool(Constants.AnimNameThrow, value);
		}

		public bool Dead
		{
			get => _animator.GetBool(Constants.AnimNameDead);
			set => _animator.SetBool(Constants.AnimNameDead, value);
		}

		public bool LevelUp
		{
			get => _animator.GetBool(Constants.AnimNameLevelUp);
			set
			{
				if (value)
				{
					_animator.SetTrigger(Constants.AnimNameLevelUp);
				}
				else
				{
					_animator.ResetTrigger(Constants.AnimNameLevelUp);
				}
			}
		}

		public AnimatorHandler(Animator animator, 
			Func<Collider[]> onAttack, 
			Action<IHitable> onBeat, 
			Action onThrow, 
			Action<bool> onPlayingLevelUp,
			Action onPullPartyPopper,
			Action<MotionSettingsInfo> onApplyMotionSettings,
			bool isMine)
		{
			_animator = animator;

			_attackEventKeys = new List<AttackEvents>();
			_throwEventKeys = new List<ThrowEvents>();
			_highFallEventKeys = new List<ViewEvents>();
			_levelUpEventKeys = new List<LevelUpEvents>();
			var behaviours = _animator.GetBehaviours<StateMachineBehaviour>();
			for (int i = 0; i < behaviours.Length; i++)
			{
				var behaviour = behaviours[i];
				if (behaviour is AttackEvents attackEvents)
				{
					_attackEventKeys.Add(attackEvents);
					attackEvents.Initialize(isMine);
					attackEvents.OnAttack += onAttack;
					attackEvents.OnBeat += onBeat;
					attackEvents.OnApplyMotionSetting += onApplyMotionSettings;
				}
				else if (behaviour is ThrowEvents throwEvents)
				{
					_throwEventKeys.Add(throwEvents);
					throwEvents.Initialize(isMine);
					throwEvents.OnThrow += onThrow;
					throwEvents.OnApplyMotionSetting += onApplyMotionSettings;
				}
				else if (behaviour is ViewEvents highFallEvents)
				{
					_highFallEventKeys.Add(highFallEvents);
					highFallEvents.Initialize(isMine);
					highFallEvents.OnApplyMotionSetting += onApplyMotionSettings;
				}
				else if (behaviour is LevelUpEvents levelUpEvents)
				{
					_levelUpEventKeys.Add(levelUpEvents);
					levelUpEvents.Initialize(isMine);
					levelUpEvents.OnPlaying += onPlayingLevelUp;
					levelUpEvents.OnPullPartyPopper += onPullPartyPopper;
					levelUpEvents.OnApplyMotionSetting += onApplyMotionSettings;
				}
			}
		}

		public void Release(
			Func<Collider[]> onAttack, 
			Action<IHitable> onHit, 
			Action onThrow,
			Action<bool> onPlayingLevelUp,
			Action onPullPartyPopper,
			Action<MotionSettingsInfo> onApplyMotionSettings)
		{
			foreach (var keys in _attackEventKeys)
			{
				keys.OnAttack -= onAttack;
				keys.OnBeat -= onHit;
				keys.OnApplyMotionSetting -= onApplyMotionSettings;
			}

			foreach (var keys in _throwEventKeys)
			{
				keys.OnThrow -= onThrow;
				keys.OnApplyMotionSetting -= onApplyMotionSettings;
			}

			foreach (var keys in _highFallEventKeys)
			{
				keys.OnApplyMotionSetting -= onApplyMotionSettings;
			}

			foreach (var keys in _levelUpEventKeys)
			{
				keys.OnPlaying -= onPlayingLevelUp;
				keys.OnPullPartyPopper -= onPullPartyPopper;
				keys.OnApplyMotionSetting -= onApplyMotionSettings;
			}
		}

		public State GetCurrentState()
		{
			var currentStateInfo = _animator.GetCurrentAnimatorStateInfo(0);
			State result = State.Neutral;

			if (currentStateInfo.IsName(Constants.AnimStateThrow) ||
				currentStateInfo.IsName(Constants.AnimStateThrowReady))
			{
				result |= State.Throw;
			}
			else if (currentStateInfo.IsName(Constants.AnimStateJump))
			{
				result |= State.Jump;
			}
			else if (currentStateInfo.IsName(Constants.AnimStateAttack01) ||
				currentStateInfo.IsName(Constants.AnimStateAttack02) ||
				currentStateInfo.IsName(Constants.AnimStateAttack03))
			{
				result |= State.Attack;
			}
			else if (currentStateInfo.IsName(Constants.AnimStateDance))
			{
				result |= State.Dance;
			}
			else if (currentStateInfo.IsName(Constants.AnimStateHighFall))
			{
				result |= State.HighFall;
			}
			else if (currentStateInfo.IsName(Constants.AnimStateDead))
			{
				result |= State.Dead;
			}

			if (_animator.GetFloat(Constants.AnimNameMoveX) != 0f ||
				_animator.GetFloat(Constants.AnimNameMoveY) != 0f)
			{
				result |= State.Move;
			}

			return result;
		}
	}
}

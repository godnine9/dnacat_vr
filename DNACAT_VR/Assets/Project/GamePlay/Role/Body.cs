using System.Collections.Generic;

namespace Ninth.GamePlay.DNACAT
{
	public class Body
	{
		private readonly Dictionary<BodyParts, BodyPart> _body;

		public BodyPart this[BodyParts bodyParts] => _body[bodyParts];

		public Body(BodyPart head, BodyPart hip, BodyPart rightHand, BodyPart leftHand)
		{
			_body = new Dictionary<BodyParts, BodyPart>
			{
				{ BodyParts.Head, head },
				{ BodyParts.Hip, hip },
				{ BodyParts.RightHand, rightHand },
				{ BodyParts.LeftHand, leftHand },
			};
		}

		public void Clear()
		{
			_body.Clear();
		}
	}
}

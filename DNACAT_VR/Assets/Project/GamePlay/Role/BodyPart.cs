using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class BodyPart : MonoBehaviour
	{
		public int ViewID
		{
			get;
			private set;
		}

		public Vector3 Position => transform.position;

		public IEquippable Equipment
		{
			get;
			private set;
		}

		public void Initialize(int viewID)
		{
			ViewID = viewID;
		}

		public void Equip(IEquippable equipment)
		{
			Equipment = equipment;
		}

		public void Unequip()
		{
			Equipment.OnUnequipped(ViewID);
			Equipment = null;
		}
	}
}

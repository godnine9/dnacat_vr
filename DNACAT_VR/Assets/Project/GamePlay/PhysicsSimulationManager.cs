using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ninth.GamePlay.DNACAT
{
	public class PhysicsSimulationManager : SceneSingleton<PhysicsSimulationManager>
	{
		[SerializeField]
		private Transform _obstacleRoot = default;

		private Scene _simulationScene;
		private PhysicsScene _physicsScene;
		private List<Transform> _obstacles;

		private Dictionary<int, Rigidbody> _simulationInstances;

		protected override void Awake()
		{
			_simulationInstances = new Dictionary<int, Rigidbody>();
		}

		private void Start()
		{
			Physics.autoSimulation = false;
			CreatePhysicsScene();
		}

		private void OnDestroy()
		{
			_obstacles.ForEach(Destroy);
			_obstacles.Clear();
		}

		private void CreatePhysicsScene()
		{
			_obstacles = new List<Transform>();
			var parameters = new CreateSceneParameters(LocalPhysicsMode.Physics3D);
			_simulationScene = SceneManager.CreateScene("Simulation", parameters);
			_physicsScene = _simulationScene.GetPhysicsScene();

			foreach (Transform obstacle in _obstacleRoot)
			{
				if (obstacle.TryGetComponent<Collider>(out _))
				{
					var simulatedObstacle = Instantiate(obstacle, obstacle.position, obstacle.rotation);
					var renderers = simulatedObstacle.GetComponentsInChildren<Renderer>();
					foreach (var renderer in renderers)
					{
						renderer.enabled = false;
					}
					SceneManager.MoveGameObjectToScene(simulatedObstacle.gameObject, _simulationScene);
					_obstacles.Add(simulatedObstacle);
				}
			}
		}

		public void Simulate(Rigidbody prefab, Vector3 startPosition, Vector3 velocity, ForceMode forceMode, int maxSimulateFrames, Action<int, Vector3> onSimulated)
		{
			if (!_simulationScene.IsValid())
			{
				return;
			}

			int instanceID = prefab.GetInstanceID();
			if (!_simulationInstances.TryGetValue(prefab.GetInstanceID(), out var dummy))
			{
				dummy = Instantiate(prefab);
				_simulationInstances.Add(instanceID, dummy);
				SceneManager.MoveGameObjectToScene(dummy.gameObject, _simulationScene);
			}

			dummy.transform.position = startPosition;
			dummy.AddForce(velocity, forceMode);

			for (var i = 0; i < maxSimulateFrames; i++)
			{
				_physicsScene.Simulate(Time.fixedDeltaTime);
				onSimulated?.Invoke(i, dummy.transform.position);
			}

			Destroy(dummy.gameObject);
			_simulationInstances.Remove(instanceID);
		}
	}
}

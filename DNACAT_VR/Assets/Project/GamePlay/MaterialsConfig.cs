using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	[CreateAssetMenu(fileName = "MaterialsConfig", menuName = "Ninth/MaterialsConfig")]
	public class MaterialsConfig : ScriptableObject
	{
		[SerializeField]
		private List<Material> _materials = default;

		public IReadOnlyList<Material> Materials => _materials;
	}
}

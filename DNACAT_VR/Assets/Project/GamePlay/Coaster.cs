using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
    public class Coaster : MonoBehaviour
    {
        public Controller TargetController;
        public void ExitCoaster()
        {
            TargetController.transform.SetParent(transform.root.parent);
            TargetController.ExitCoaster();
            this.gameObject.GetComponent<Animator>().Play("Stop");
        }
    }
}

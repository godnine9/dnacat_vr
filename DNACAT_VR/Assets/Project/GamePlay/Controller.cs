using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	public class Controller : HitableObject, IPunObservable, IHitable
	{
		private const float _triggerStayFreq = 0.8f;

		public Action<int> OnKilledEnemy;
		public Action<ItemType, int> OnGetItem;

		[SerializeField]
		private AudioListener _audioListener = default;
		[SerializeField]
		private PlayerHUD _playerHUD = default;
		[SerializeField]
		private Animator _animator = default;
		[SerializeField]
		private CharacterController _controller = default;
		[SerializeField]
		private BodyPart _head = default;
		[SerializeField]
		private BodyPart _hip = default;
		[SerializeField]
		private BodyPart _leftHand = default;
		[SerializeField]
		private BodyPart _rightHand = default;
		[SerializeField]
		private Transform _throwView = default;
		[SerializeField]
		private float _speed = default;
		[SerializeField]
		private float _jumpHeight = default;
		[SerializeField]
		private Renderer _faceRenderer = default;
		[SerializeField]
		private Renderer _bodyRenderer = default;
		[SerializeField]
		private BoxCollider _hitBox = default;
		[SerializeField]
		private PhysicSimulation _throwPridictionLine = default;
		[SerializeField]
		private GameObject _weapon = default;
		[SerializeField]
		private float _projectileForce = 300f;
		[SerializeField]
		private float _highFallThreshold = -8f;
		[SerializeField]
		private Transform _partyPopper = default;

		private Transform _camera;
		private Vector3 _inputDir;
		private Vector3 _movement;
		private Vector3 _velocity;
		private CollisionFlags _collisionFlags;
		private Vector3 _groudCheckHalfExtents;
		private Vector3 _groundCheckExtents;
		private AnimatorHandler _animatorHandler;
		private Dictionary<int, InteractionBase> _stayingInteractions;
		private float _currentTriggerStayFreq = 0f;
		private bool _movable;
		private bool _IsLevelUp;
		private bool _IsInCoster;
		private float _ExitCosterForce;
		private int _testAtk = 3;
		private Projectile _projectile;
		private object _triggerFlagsLocker = new object();
		private Coroutine _deadProcess;

		public Body Body
		{
			get;
			private set;
		}

		public bool HasWeapon
		{
			get => _weapon.activeSelf;
			set => photonView.RpcSecure(nameof(RPC_HasWeapon), RpcTarget.All, true, value);
		}

		public float AttackRange
		{
			get
			{
				//var quipment = Body[BodyParts.RightHand].Equipment;
				//return quipment != null ? quipment.Range : 0f;

				return 5f;
			}
		}

		public bool IsGrounded => Physics.CheckBox(transform.position, _groudCheckHalfExtents, transform.rotation, LayerMask.GetMask("Environment"));

		public Transform ThrowView => _throwView;

		public bool IsMine => photonView.IsMine;

		public TriggerFlags TriggerFlags
		{
			get;
			private set;
		}

		public AnimatorHandler.State AnimatorState => _animatorHandler.GetCurrentState();

		private IInputHandler InputHandler => InputManager.Instance.Handler;

		private CameraManager CameraManager => CameraManager.Instance;

		private PlayerInfo PlayerInfo => GameManager.Instance.LocalPlayerInfo;

		private PhysicsSimulationManager PhysicsSimulationManager => PhysicsSimulationManager.Instance;

		public void SetBodyMaterial(int materialIndex)
		{
			photonView.RpcSecure(nameof(RPC_SetBodyMaterial), RpcTarget.All, true, materialIndex);
		}

		[PunRPC]
		private void RPC_SetBodyMaterial(int materialIndex)
		{
			_bodyRenderer.material = new Material(GameManager.Instance.CatMaterials[materialIndex]);
		}

		public void SetProjectile(Projectile projectile)
		{
			_projectile = projectile;
		}

		public void SetRenderersLayer(int layer)
		{
			_faceRenderer.gameObject.layer = layer;
			_bodyRenderer.gameObject.layer = layer;
		}

		public bool EnterCoaster()
		{
			if (_IsInCoster)
			{
				return false;
			}
			else
			{
				GetComponent<Animator>().SetBool("Coaster",true);
				_IsInCoster = true;
				_ExitCosterForce = 0;
				return true;
			}
		}

		public void ExitCoaster()
		{
			_IsInCoster = false;
			_ExitCosterForce = 50;
			if (IsMine)
			{
				GetComponent<Animator>().SetBool("Coaster", false);
				DontDestroyOnLoad(gameObject);
			}
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
		{
			if (stream.IsWriting)
			{
				stream.SendNext(_animatorHandler.Attack);
				stream.SendNext(_animatorHandler.Expression);
				//stream.SendNext(_animatorHandler.HighFall);
				stream.SendNext(_animatorHandler.Dead);
			}
			else
			{
				_animatorHandler.Attack = (bool)stream.ReceiveNext();
				_animatorHandler.Expression = (int)stream.ReceiveNext();
				//_animatorHandler.HighFall = (bool)stream.ReceiveNext();
				_animatorHandler.Dead = (bool)stream.ReceiveNext();
			}
		}

		public void SetCamera(Transform cameraTrans)
		{
			_camera = cameraTrans;
		}

		public void SendShowChatBubble(string msg)
		{
			if (IsMine)
			{
				photonView.RpcSecure(nameof(RPC_ShowChatBubble), RpcTarget.All, true, msg);
			}
		}

		public void LevelUp(int level)
		{
			_animatorHandler.LevelUp = true;
			photonView.RpcSecure(nameof(RPC_LevelUp), RpcTarget.All, true);
		}

		private void Awake()
		{
			_groudCheckHalfExtents = _controller.radius * Vector3.one;
			_groudCheckHalfExtents.y = 0.25f;
			_groundCheckExtents = CalculateGroundCheckSize(_groudCheckHalfExtents);

			_animatorHandler = new AnimatorHandler(_animator, 
				Attack, 
				OnBeatEnemy, 
				Throw, 
				PlayingLevelUp, 
				PullPartyPropper, 
				ApplyMotionSettings, 
				IsMine);

			_playerHUD.Initialize(photonView.Owner.NickName, IsMine);
			_audioListener.enabled = IsMine;
			_rightHand.Initialize(photonView.ViewID);
			Body = new Body(_head, _hip, _rightHand, _leftHand);
			_stayingInteractions = new Dictionary<int, InteractionBase>();
			_throwPridictionLine.Visible = false;
			_movable = true;
			HasWeapon = false;

			int matIndex = (int)photonView.InstantiationData[0];
			SetBodyMaterial(matIndex);
		}

		private void OnDestroy()
		{
			OnGetItem = null;
			OnKilledEnemy = null;
			Body.Clear();
			_animatorHandler.Release(
				Attack, 
				OnBeatEnemy, 
				Throw, 
				PlayingLevelUp, 
				PullPartyPropper, 
				ApplyMotionSettings);
		}

		private void Update()
		{
			if (IsMine && !_IsInCoster)
			{
				if (InputHandler.PickUpItem())
				{
					PickUpItem();
				}
				NeutralMovement();
				FallToDie();
			}
		}

		private void LateUpdate()
		{
			if (IsMine)
			{
				_throwPridictionLine.Visible = AnimatorState.HasFlag(AnimatorHandler.State.Throw);
				if (InputHandler.Throw() && _projectile)
				{
					var direction = _camera.forward;
					var position = Body[BodyParts.LeftHand].Position + direction.normalized;
					_throwPridictionLine.ResetLineCount();
					PhysicsSimulationManager.Simulate(_projectile.Rigidbody, position, _projectileForce * direction, ForceMode.Force, _throwPridictionLine.MaxFrames, _throwPridictionLine.SetLinePosition);
				}
			}
		}

		private void FallToDie()
        {
			if(transform.position.y <= -35)
            {
				GameManager.Instance.Reborn(transform);
			}
        }

		//private void OnGUI()
		//{
		//	if (IsMine)
		//	{
		//		float y = 40f;
		//		float space = 30f;
		//
		//		GUI.color = Color.black;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"Velocity:{_velocity}");
		//		y += space;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"Input:{_inputDir}");
		//		y += space;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"Collision Flags:{_collisionFlags}");
		//		y += space;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"Momement:{_movement}");
		//		y += space;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"TriggerFlags:{TriggerFlags}");
		//		y += space;
		//		GUI.Label(new Rect(10f, y, 300f, 30f), $"AnimatorState:{AnimatorState}");
		//
		//		y += space;
		//		if (GUI.Button(new Rect(10f, y, 300f, 30f), "Reset All"))
		//		{
		//			GameManager.Instance.LocalPlayerInfo.ResetAll();
		//		}
		//	}
		//}
		//
		//private void OnDrawGizmos()
		//{
		//	Gizmos.color = Color.red;
		//	Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
		//	Gizmos.DrawWireCube(Vector3.zero, _groundCheckExtents);
		//}

		private void OnTriggerEnter(Collider collider)
		{
			if (IsInteractable(collider, out var interaction))
			{
				if (interaction.TriggerFlags != TriggerFlags.None)
				{
					lock (_triggerFlagsLocker)
					{
						TriggerFlags |= interaction.TriggerFlags;
						_stayingInteractions[collider.GetInstanceID()] = interaction;
					}
				}
				interaction.OnEntered(photonView.ViewID);
			}
		}

		private void OnTriggerStay(Collider collider)
		{
			_currentTriggerStayFreq += Time.deltaTime;
			if (_currentTriggerStayFreq >= _triggerStayFreq)
			{
				ResetTriggerStayFreq();
				int instanceID = collider.GetInstanceID();
				if (_stayingInteractions.TryGetValue(instanceID, out var interaction))
				{
					if (interaction.TriggerFlags != TriggerFlags.None)
					{
						TriggerFlags |= interaction.TriggerFlags;
						interaction.OnStayed(photonView.ViewID);
					}
				}
			}
		}

		private void OnTriggerExit(Collider collider)
		{
			if (IsInteractable(collider, out var interaction))
			{
				if (interaction.TriggerFlags != TriggerFlags.None)
				{
					//ResetTriggerStayFreq();
					TriggerFlags ^= interaction.TriggerFlags;
				}
				interaction.OnExited(photonView.ViewID);
				_stayingInteractions.Remove(collider.GetInstanceID());
			}
		}

		private void NeutralMovement()
		{
			bool isNeutral = AnimatorState.HasFlag(AnimatorHandler.State.Neutral);
			bool isThrowing = AnimatorState.HasFlag(AnimatorHandler.State.Throw);
			bool isAttacking = AnimatorState.HasFlag(AnimatorHandler.State.Attack);
			bool isDead = AnimatorState.HasFlag(AnimatorHandler.State.Dead);
			bool isHighFall = AnimatorState.HasFlag(AnimatorHandler.State.HighFall);
			bool isMoving = _animatorHandler.Move != Vector3.zero;
			bool isGrounded = IsGrounded;

			_animatorHandler.HighFall = _velocity.y < _highFallThreshold;
			_animatorHandler.IsGrounded = isGrounded;
			if(_ExitCosterForce < 0 && isGrounded)
            {
				_ExitCosterForce = 0;
            }
			if (isGrounded)
			{
				if (_velocity.y < 0f)
				{
					_velocity.y = Constants.GroundedAccelerate;
				}

				if (isNeutral && !_animatorHandler.HighFall && !_animator.IsInTransition(0) && !isHighFall)
				{
					if (InputHandler.Jump())
					{
						// v = f(h) = Sqrt(2gh)
						_velocity.y = Mathf.Sqrt(-2f * Constants.Gravity * _jumpHeight);
						_animatorHandler.Expression = 0;
					}

					if (isMoving)
					{
						_animatorHandler.Expression = 0;
					}
					else
					{
						int expression = InputHandler.Express();
						if (expression > 0)
						{
							_animatorHandler.Expression = expression;
						}
					}
				}

				if (isMoving)
				{
					_animatorHandler.Expression = 0;
				}
			}

			_animatorHandler.Attack = InputHandler.Attack() && HasWeapon;
			_animatorHandler.Throw = InputHandler.Throw() && _projectile;

			if (!isHighFall)
			{
				int cameraIndex = Convert.ToInt32(_animatorHandler.Throw);
				CameraManager.SwitchFollow(CameraManager.StartupView, cameraIndex);
			}

			if (isThrowing || isAttacking || _IsLevelUp)
			{
				_inputDir = Vector3.zero;
				_animatorHandler.Expression = 0;
			}
			else
			{
				var move = InputHandler.Move();
				_inputDir = new Vector3(move.x != 0 ? move.x > 0 ? 1 : -1 : 0, 0, move.z != 0 ? move.z > 0 ? 1 : -1 : 0);
			}

			_movement = _movable ? transform.TransformDirection(_inputDir).normalized : Vector3.zero;
			_movement.y = 0f;
			_animatorHandler.Move = _inputDir;

			var forward = _camera ? _camera.forward : Vector3.zero;
			forward.y = 0f;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(forward), 0.2f);

			_movement *= 0.1f;
			_velocity.y += Constants.Gravity * Time.deltaTime;
			_velocity = new Vector3(0, _velocity.y, 0);
			if(_ExitCosterForce > 0)
            {
				_velocity += transform.forward * _ExitCosterForce;
				_ExitCosterForce -= Time.deltaTime * 25;
			}
			_collisionFlags = _controller.Move(_velocity * Time.deltaTime);
			transform.position += _movement* _speed;
		}

		private Collider[] Attack()
		{
			if (IsMine)
			{
				Vector3 center = _hitBox.transform.position;
				Vector3 size = _hitBox.size;
				Quaternion orientation = _hitBox.transform.rotation;

				size.z *= AttackRange;
				return Physics.OverlapBox(center, size * 0.5f, orientation, Constants.LayerMask_Enemy, QueryTriggerInteraction.Collide);
			}
			return null;
		}

		private void OnBeatEnemy(IHitable enemy)
		{
			if (IsMine && enemy.OnHit(5, photonView.ViewID))
			{
				if (enemy is Enemy typedEnemy)
				{
					OnKilledEnemy?.Invoke(typedEnemy.Data.EXP);
				}
			}
		}

		public override bool OnHit(int atk, int targetID)
		{
			if (photonView.ViewID != targetID || !IsMine)
			{
				return false;
			}
			if (!_isHitable)
			{
				return false;
			}
			photonView.RpcSecure(nameof(RPC_OnHit), RpcTarget.All, true);
			PlayerInfo.AddHP(-atk);
			bool isDead = PlayerInfo.HP.Value <= 0;
			if (isDead)
			{
				Die();
			}
			return isDead;
		}

		private void Die()
		{
			if (_deadProcess != null)
			{
				StopCoroutine(_deadProcess);
			}
			_deadProcess = StartCoroutine(DeadProcess());
		}

		private IEnumerator DeadProcess()
		{
			photonView.RpcSecure(nameof(RPC_OnDead), RpcTarget.All, true);
			yield return new WaitForSeconds(6f);
			photonView.RpcSecure(nameof(RPC_OnReborn), RpcTarget.All, true);
		}

		public override void OnDisable()
		{
			base.OnDisable();
			PhotonNetwork.OpCleanRpcBuffer(photonView);
		}

		private void Throw()
		{
			if (IsMine)
			{
				var direction = _camera.forward;
				var position = Body[BodyParts.LeftHand].Position + direction.normalized;
				var rotation = _camera.rotation;
				var projectileObject = PhotonNetwork.Instantiate(_projectile.gameObject.name, position, rotation);
				if (projectileObject.TryGetComponent<Projectile>(out var projectile))
				{
					projectile.OnHitEnemy += OnBeatEnemy;
					projectile.Launch(direction, _projectileForce);
				}
			}
		}

		private void PlayingLevelUp(bool isPlaying)
		{
			if (!IsMine)
			{
				return;
			}
			_partyPopper.gameObject.SetActive(isPlaying);
			_IsLevelUp = isPlaying;
			if (isPlaying)
			{
				CameraManager.SwitchCamera(CameraManager.View.ThirdPerson);
			}
			else
			{
				CameraManager.ResetCamera();
			}
		}

		private void PullPartyPropper()
		{
			photonView.RpcSecure(nameof(RPC_PullPartyPropper), RpcTarget.All, true);
		}

		private void ApplyMotionSettings(MotionSettingsInfo info)
		{
			_movable = info.IsMovable;
			_isHitable = info.IsHitable;
		}

		private void PickUpItem()
		{
			foreach (var interaction in _stayingInteractions.Values)
			{
				if (interaction is IPickable item)
				{
					_stayingInteractions.Remove(interaction.Collider.GetInstanceID());
					OnGetItem?.Invoke(item.Type, item.Amount);
					item.OnPicked();
					TriggerFlags ^= interaction.TriggerFlags;
					ResetTriggerStayFreq();
					break;
				}
			}
		}

		private void ResetTriggerStayFreq()
		{
			_currentTriggerStayFreq = 0f;
		}

		private Vector3 CalculateGroundCheckSize(Vector3 halfExtents)
		{
			float height = halfExtents.y;
			Vector3 size = halfExtents * 2;
			size.y = height;
			return size;
		}

		private bool IsInteractable(Collider collider)
		{
			return IsMine && collider.CompareTag(Constants.Tag_Interaction);
		}

		private bool IsInteractable(Collider collider, out InteractionBase interaction)
		{
			interaction = null;
			return IsInteractable(collider) && collider.TryGetComponent(out interaction);
		}

		[PunRPC]
		private void RPC_ShowChatBubble(string msg)
		{
			_playerHUD.ShowChatBubble(msg);
		}

		[PunRPC]
		private void RPC_HasWeapon(bool value)
		{
			_weapon.SetActive(value);
		}

		[PunRPC]
		private void RPC_OnHit()
		{
			AudioManager.PlaySound("HitEnemy");
			VFXManager.Instance.SpawnEffect("VFX_Hit", _hip.Position, Quaternion.identity);
		}

		[PunRPC]
		private void RPC_OnDead()
		{
			_animatorHandler.Dead = true;
			_isHitable = false;
			_movable = false;
		}

		[PunRPC]
		private void RPC_OnReborn()
		{
			GameManager.Instance.Reborn(transform);
			_animatorHandler.Dead = false;
			PlayerInfo.FillHP();
			_isHitable = true;
			_movable = true;
			HasWeapon = false;
		}

		[PunRPC]
		private void RPC_LevelUp()
		{
			VFXManager.Instance.SpawnEffect("VFX_LevelUP", transform.position, Quaternion.Euler(90, 0, 0));
		}

		[PunRPC]
		private void RPC_PullPartyPropper()
		{
			VFXManager.Instance.SpawnEffect("VFX_PartyPopper", _partyPopper.position, Quaternion.Euler(-90, 0, 0));
		}
	}
}

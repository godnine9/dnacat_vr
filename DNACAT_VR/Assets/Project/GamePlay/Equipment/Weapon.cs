using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	[RequireComponent(typeof(PhotonView))]
	public class Weapon : MonoBehaviour, IEquippable
	{
		[SerializeField]
		private PhotonView _photonView = default;
		[SerializeField]
		private Vector3 _pickUpLocalPosition = Vector3.zero;
		[SerializeField]
		private Vector3 _pickUpLocalRotation = Vector3.zero;

		private Quaternion _pickUpLocalRotationQuat;

		public BodyParts BodyParts
		{
			get;
			private set;
		}

		public float Range
		{
			get;
			private set;
		}

		public WeaponProvider Provider
		{
			get;
			private set;
		}

		public void Initialize(WeaponProvider provider, int range)
		{
			Provider = provider;
			Range = range;
		}

		public bool BelongsTo(WeaponProvider provider)
		{
			return Provider.GetInstanceID() == provider.GetInstanceID();
		}

		public void OnEquipped(int viewID, BodyParts bodyParts)
		{
			_photonView.RpcSecure(nameof(RPC_OnEquipped), RpcTarget.All, true, viewID, bodyParts);
		}

		public void OnUnequipped(int viewID)
		{
			_photonView.RpcSecure(nameof(RPC_OnUnequipped), RpcTarget.All, true, viewID);
		}

		private void Awake()
		{
			_pickUpLocalRotationQuat = Quaternion.Euler(_pickUpLocalRotation);
		}

		[PunRPC]
		private void RPC_OnEquipped(int viewID, BodyParts bodyParts)
		{
			var parentView = PhotonNetwork.GetPhotonView(viewID);
			if (!parentView.TryGetComponent<Controller>(out var controller))
			{
				Debug.LogError($"[{nameof(Weapon)}.{nameof(RPC_OnEquipped)}] This is not a Controller.");
				return;
			}

			BodyParts = bodyParts;
			transform.parent = controller.Body[bodyParts].transform;
			transform.localPosition = _pickUpLocalPosition;
			transform.localRotation = _pickUpLocalRotationQuat;
		}

		[PunRPC]
		private void RPC_OnUnequipped(int viewID)
		{
			var parentView = PhotonNetwork.GetPhotonView(viewID);
			if (!parentView.TryGetComponent<Controller>(out var controller))
			{
				Debug.LogError($"[{nameof(Weapon)}.{nameof(RPC_OnUnequipped)}] This is not a Controller.");
				return;
			}

			if (_photonView.IsMine)
			{
				PhotonNetwork.Destroy(_photonView);
			}
		}
	}
}

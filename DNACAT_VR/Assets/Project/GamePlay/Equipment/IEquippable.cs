﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public interface IEquippable
	{
		BodyParts BodyParts { get; }

		public float Range { get; }

		void OnEquipped(int viewID, BodyParts bodyParts);

		void OnUnequipped(int viewID);
	}
}

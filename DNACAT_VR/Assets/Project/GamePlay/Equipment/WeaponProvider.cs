﻿using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	public class WeaponProvider : InteractionBase
	{
		[SerializeField]
		private int _weaponID = 0;
		[SerializeField]
		private Collider _collider = null;

		private WeaponData _data;

		public override Collider Collider => _collider;

		public override TriggerFlags TriggerFlags => TriggerFlags.WeaponProvider;

		private void Awake()
		{
			if (!WeaponDataSet.Instance.TryGetData(_weaponID, out _data))
			{
				Debug.LogError($"Weapon ID not found:{_weaponID}");
			}
		}

		public override void OnEntered(int viewID)
		{
			var controllerView = PhotonNetwork.GetPhotonView(viewID);
			if (!controllerView.TryGetComponent<Controller>(out var controller))
			{
				Debug.LogError($"[{nameof(WeaponProvider)}.{nameof(OnEntered)}] This is not a Controller.");
				return;
			}

			controller.HasWeapon = true;

			//var targetBodyPart = controller.Body[_bodyParts];
			//if (targetBodyPart.Equipment != null && targetBodyPart.Equipment is Weapon currentlyWeapon)
			//{
			//	if (!currentlyWeapon.BelongsTo(this))
			//	{
			//		targetBodyPart.Unequip();
			//	}
			//	else
			//	{
			//		Debug.LogWarning($"[{nameof(Weapon)}.{nameof(OnEntered)}] Already equipped same weapon.");
			//		return;
			//	}
			//}

			//var weaponObject = PhotonNetwork.Instantiate(_data.Prefab, transform.position, transform.rotation);
			//if (weaponObject.TryGetComponent<Weapon>(out var weapon))
			//{
			//	weapon.Initialize(this, _data.Range);
			//	weapon.OnEquipped(viewID, _bodyParts);
			//	targetBodyPart.Equip(weapon);
			//}
		}
	}
}

﻿using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	[RequireComponent(typeof(BoxCollider))]
	public class ProjectileProvider : InteractionBase
	{
		[SerializeField]
		private Collider _collider = null;
		[SerializeField]
		private Projectile _projectile = default;

		public override Collider Collider => _collider;

		public override TriggerFlags TriggerFlags => TriggerFlags.ProjectileProvider;

		public override void OnEntered(int viewID)
		{
			var controllerView = PhotonNetwork.GetPhotonView(viewID);
			if (controllerView.TryGetComponent<Controller>(out var controller))
			{
				controller.SetProjectile(_projectile);
			}
			else
			{
				Debug.LogError($"[{nameof(ProjectileProvider)}.{nameof(OnEntered)}] This is not a Controller.");
			}
		}
	}
}
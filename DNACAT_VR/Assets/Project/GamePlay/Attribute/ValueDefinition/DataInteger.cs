﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class DataInteger : PrefsValue<int>
	{
		public DataInteger(string key) : base(key)
		{
		}

		public override int Value
		{
			get => PlayerPrefs.GetInt(Key);
			set => PlayerPrefs.SetInt(Key, value);
		}

		public override void Save() => PlayerPrefs.Save();
	}
}
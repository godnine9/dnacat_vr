using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public static class PlayerLevelPreset
	{
		public static IReadOnlyDictionary<int, PlayerLevelData> SearchByLevel
		{
			get;
			private set;
		}

		public static IReadOnlyList<PlayerLevelData> OrderByEXP
		{
			get;
			private set;
		}

		public static void Initialize(IReadOnlyDictionary<int, PlayerLevelData> dataContainer)
		{
			if (dataContainer == null)
			{
				Debug.LogError("PlayerLevelData is not loaded yet.");
			}
			else
			{
				SearchByLevel = dataContainer.ToDictionary(pair => pair.Value.Level, pair => pair.Value);
				OrderByEXP = dataContainer.Values.OrderByDescending(data => data.EXP).ToList();
			}
		}
	}
}

using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class DataFloat : PrefsValue<float>
	{
		public DataFloat(string key) : base(key)
		{
		}

		public override float Value
		{
			get => PlayerPrefs.GetFloat(Key);
			set => PlayerPrefs.SetFloat(Key, value);
		}

		public override void Save() => PlayerPrefs.Save();
	}
}

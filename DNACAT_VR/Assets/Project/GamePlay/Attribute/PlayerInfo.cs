using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class PlayerInfo : IDisposable
	{
		public event Action<int, int, int> OnUpdateEXP;
		public event Action<int> OnLevelUp;
		public event Action<ItemType, int> OnUpdateItemCount;
		public event Action<float, float> OnUpdateHP;
		public event Action<float, float> OnUpdateMP;

		private readonly Dictionary<ItemType, ItemInfo> _items;

		public PrefsValue<int> EXP { get; }

		public PrefsValue<float> HP { get; }

		public PrefsValue<float> MP { get; }

		public IReadOnlyDictionary<ItemType, ItemInfo> Items => _items;

		public int CurrentLevel => CurrentLevelData.Level;

		public int MaxHP => CurrentLevelData.MaxHP;

		public int MaxMP => CurrentLevelData.MaxMP;

		public int MaxEXP => !IsMaxLevel ? NextLevelData.EXP - CurrentLevelData.EXP : 0;

		public int CurrentEXP
		{
			get;
			private set;
		}

		public PlayerLevelData NextLevelData
		{
			get;
			private set;
		}

		public PlayerLevelData CurrentLevelData
		{
			get;
			private set;
		}

		public bool IsMaxLevel => NextLevelData == null;

		public PlayerInfo()
		{
			EXP = new DataInteger($"[{nameof(PlayerInfo)}]{nameof(EXP)}");
			ResetLevelData();
			CurrentEXP = EXP.Value - CurrentLevelData.EXP;

			HP = new DataFloat($"[{nameof(PlayerInfo)}]{nameof(HP)}");
			MP = new DataFloat($"[{nameof(PlayerInfo)}]{nameof(MP)}");

			if (HP.Value <= 0)
			{
				HP.Value = CurrentLevelData.MaxHP;
			}

			if (MP.Value <= 0)
			{
				MP.Value = CurrentLevelData.MaxMP;
			}

			_items = new Dictionary<ItemType, ItemInfo>();
			foreach (int itemID in Enum.GetValues(typeof(ItemType)))
			{
				ItemType type = (ItemType)itemID;
				_items[type] = new ItemInfo(itemID);
			}

			PlayerPrefs.Save();
		}

		public void FillHP()
		{
			HP.Value = MaxHP;
			OnUpdateHP?.Invoke(HP.Value, MaxHP);
		}

		public void AddHP(int value)
		{
			HP.Value = Mathf.RoundToInt(Mathf.Clamp(HP.Value + value, 0f, MaxHP));
			OnUpdateHP?.Invoke(HP.Value, MaxHP);
		}

		public void ResetAll()
		{
			EXP.Value = 0;
			CurrentEXP = 0;
			ResetLevelData();
			HP.Value = MaxHP;
			MP.Value = MaxMP;

			OnUpdateEXP?.Invoke(CurrentEXP, MaxEXP, EXP.Value);
			OnUpdateHP?.Invoke(HP.Value, MaxHP);
			OnUpdateMP?.Invoke(MP.Value, MaxMP);

			foreach (var itemInfo in _items.Values)
			{
				itemInfo.Count.Value = 0;
				OnUpdateItemCount?.Invoke((ItemType)itemInfo.ItemID, itemInfo.Count.Value);
			}
			PlayerPrefs.Save();
		}

		public void GetEXP(int exp)
		{
			if (!IsMaxLevel)
			{
				EXP.Value += exp;
				UpdateEXP();
			}
		}

		public void GetItem(ItemType type, int addCount)
		{
			if (_items.TryGetValue(type, out var info))
			{
				info.Count.Value += addCount;
				OnUpdateItemCount?.Invoke(type, info.Count.Value);
				PlayerPrefs.Save();
			}
		}

		public bool SellItem(ItemType type, int count)
		{
			if (_items.TryGetValue(type, out var info) && info.Count.Value > 0)
			{
				if (ItemDataSet.Instance.TryGetData((int)type, out var itemData))
				{
					info.Count.Value -= count;
					int earned = itemData.Price * count;
					_items[ItemType.Coin].Count.Value += earned;
					PlayerPrefs.Save();
					return true;
				}
			}
			return false;
		}

		private void UpdateEXP()
		{
			CurrentEXP = EXP.Value - CurrentLevelData.EXP;

			// Check level up
			int expDiff = EXP.Value - NextLevelData.EXP;
			if (expDiff >= 0)
			{
				CurrentEXP = expDiff;
				ResetLevelData();
				OnLevelUp?.Invoke(CurrentLevel);

				HP.Value = MaxHP;
				MP.Value = MaxMP;
				OnUpdateHP?.Invoke(HP.Value, MaxHP);
				OnUpdateMP?.Invoke(MP.Value, MaxMP);
			}

			OnUpdateEXP?.Invoke(CurrentEXP, MaxEXP, EXP.Value);
			PlayerPrefs.Save();
		}

		private void ResetLevelData()
		{
			NextLevelData = null;
			foreach (var data in PlayerLevelPreset.OrderByEXP)
			{
				if (EXP.Value >= data.EXP)
				{
					CurrentLevelData = data;
					break;
				}
				else
				{
					NextLevelData = data;
				}
			}
		}

		public void Dispose()
		{
			OnUpdateEXP = null;
			OnLevelUp = null;
			_items.Clear();
		}
	}
}

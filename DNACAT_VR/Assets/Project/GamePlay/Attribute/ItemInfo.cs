﻿namespace Ninth.GamePlay.DNACAT
{
	public class ItemInfo
	{
		public int ItemID { get; }

		public PrefsValue<int> Count { get; }

		public ItemInfo(int itemID)
		{
			ItemID = itemID;
			Count = new DataInteger($"[{nameof(ItemInfo)}{itemID}]{nameof(Count)}");
		}
	}
}
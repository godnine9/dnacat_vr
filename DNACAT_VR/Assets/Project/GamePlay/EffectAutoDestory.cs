using UnityEngine;
namespace Ninth.GamePlay.DNACAT
{
	public class EffectAutoDestory : MonoBehaviour
	{
		public void OnParticleSystemStopped()
		{
			Destroy(gameObject);
		}
	}
}

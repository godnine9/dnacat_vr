﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class Preferences : Singleton<Preferences>
	{
		public PrefsValue<string> UserName;
		public InputOptions InputOptions;

		protected override void OnInitialized()
		{
			InputOptions = new InputOptions();
			UserName = new BuiltinPrefsString($"[{nameof(Preferences)}]{nameof(UserName)}");
		}
	}
}

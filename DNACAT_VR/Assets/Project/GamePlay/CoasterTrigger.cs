using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class CoasterTrigger : MonoBehaviour
	{
		public Coaster _Coaster;
		public Animator CoasterAnim;

		public void OnTriggerEnter(Collider other)
		{
			if (other.TryGetComponent<Controller>(out var controller))
			{
				if (controller.photonView.IsMine)
				{
					if (controller.EnterCoaster())
					{
						controller.transform.SetParent(_Coaster.transform);
						CoasterAnim.Play("Play");
						_Coaster.TargetController = controller;
						controller.transform.localPosition = Vector3.zero;
						controller.transform.localEulerAngles = Vector3.zero;
					}
				}
			}
		}
	}
}

using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
    public class LevelUpEvents : AnimatorStateEvents<EventKeyFrame>
    {
		public event Action<bool> OnPlaying;
		public event Action OnPullPartyPopper;

		protected override void OnEnter(Animator animator)
		{
			OnPlaying?.Invoke(true);
		}

		protected override void OnExit(Animator animator)
		{
			OnPlaying?.Invoke(false);
		}

		protected override void OnTrigger(Animator animator, EventKeyFrame keyFrame)
		{
			OnPullPartyPopper?.Invoke();
		}
	}
}

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

namespace Ninth.GamePlay.DNACAT
{
	public abstract class AnimatorStateEvents<T> : StateMachineBehaviour where T : EventKeyFrame
	{
		public Action<MotionSettingsInfo> OnApplyMotionSetting;

		[SerializeField]
		protected MotionSettingsInfo _motionSettings = default;
		[SerializeField]
		protected List<T> _keyFrames = default;

		protected bool _isMine;
		private int _triggeredCount;
		private bool _isInit;

		public void Initialize(bool isMine)
		{
			_isInit = true;
			_isMine = isMine;
		}

		protected virtual void OnEnter(Animator animator)
		{
		}

		protected virtual void OnUpdate(Animator animator)
		{
		}

		protected virtual void OnExit(Animator animator)
		{
		}

		protected virtual void OnTrigger(Animator animator, EventKeyFrame keyFrame)
		{
		}

		public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (!_isInit)
			{
				return;
			}

			OnApplyMotionSetting?.Invoke(_motionSettings);
			OnEnter(animator);
			_triggeredCount = 0;
		}

		public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (!_isInit)
			{
				return;
			}

			for (int i = _triggeredCount; i < _keyFrames.Count; i++)
			{
				var keyFrame = _keyFrames[i];
				if (stateInfo.normalizedTime >= keyFrame._normalizedTime)
				{
					OnTrigger(animator, keyFrame);
					_triggeredCount++;
				}
			}
			OnUpdate(animator);
		}

		public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
			if (!_isInit)
			{
				return;
			}

			OnApplyMotionSetting?.Invoke(MotionSettingsInfo.Default);
			OnExit(animator);
			_triggeredCount = 0;
		}

		#region Useless
		public sealed override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
		}

		public sealed override void OnStateMachineEnter(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
			base.OnStateMachineEnter(animator, stateMachinePathHash, controller);
		}

		public sealed override void OnStateMachineExit(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
		}

		public sealed override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		public sealed override void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		public sealed override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		public sealed override void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}
		#endregion
	}

	[Serializable]
	public struct MotionSettingsInfo
	{
		public static MotionSettingsInfo Default = new MotionSettingsInfo
		{
			IsMovable = true,
			IsHitable = true,
		};

		[SerializeField]
		public bool IsMovable;
		[SerializeField]
		public bool IsHitable;
	}
}

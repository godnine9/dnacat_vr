﻿using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	[Serializable]
	public class EventKeyFrame
	{
		[SerializeField]
		public string Name;

		[SerializeField, Range(0f, 1f)]
		public float _normalizedTime;
	}
}

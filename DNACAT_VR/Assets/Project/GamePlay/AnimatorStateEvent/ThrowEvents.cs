﻿using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class ThrowEvents : AnimatorStateEvents<EventKeyFrame>
	{
		public event Action OnThrow;

		protected override void OnTrigger(Animator animator, EventKeyFrame keyFrame)
		{
			OnThrow();
		}
	}
}
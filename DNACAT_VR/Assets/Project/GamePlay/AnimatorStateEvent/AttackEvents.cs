﻿using Photon.Pun;
using System;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class AttackEvents : AnimatorStateEvents<EventKeyFrame>
	{
		public event Func<Collider[]> OnAttack;
		public event Action<HitableObject> OnBeat;
		private Collider[] _hitTargets;

		protected override void OnEnter(Animator animator)
		{
			_hitTargets = OnAttack();
		}

		protected override void OnExit(Animator animator)
		{
			_hitTargets = null;
		}

		protected override void OnTrigger(Animator animator, EventKeyFrame keyFrame)
		{
			if (_hitTargets != null)
			{
				foreach (var hitTarget in _hitTargets)
				{
					if (hitTarget)
					{
						if (hitTarget.TryGetComponent<HitableObject>(out var target))
						{
							OnBeat?.Invoke(target);
						}
					}
				}
			}
		}
	}

	public interface IHitable
	{
		bool OnHit(int atk, int sourceViewID);
	}

	public abstract class HitableObject : MonoBehaviourPunCallbacks, IHitable
	{
		protected bool _isHitable = true;

		public abstract bool OnHit(int atk, int sourceViewID);
	}
}
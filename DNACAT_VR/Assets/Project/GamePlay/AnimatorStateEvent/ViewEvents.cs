using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public class ViewEvents : AnimatorStateEvents<EventKeyFrame>
	{
		[SerializeField]
		private CameraManager.View _view  = default;

		protected override void OnEnter(Animator animator)
		{
			if (_isMine)
			{
				CameraManager.Instance.SwitchCamera(_view);
			}
		}

		protected override void OnExit(Animator animator)
		{
			if (_isMine)
			{
				CameraManager.Instance.ResetCamera();
			}
		}
	}
}

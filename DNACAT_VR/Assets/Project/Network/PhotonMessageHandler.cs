using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

namespace Ninth.GamePlay.DNACAT
{
	public class PhotonMessageHandler : MonoBehaviourPunCallbacks
	{
		public bool JoinedRoom
		{
			get;
			private set;
		}

		public bool ConnectedToMaster
		{
			get;
			private set;
		}

		public override void OnConnected()
		{
			Debug.Log("Connected.");
		}

		public override void OnDisconnected(DisconnectCause cause)
		{
			Debug.Log("Disconnected.");
			ConnectedToMaster = false;
		}

		public override void OnConnectedToMaster()
		{
			Debug.Log("Connected to Master.");
			ConnectedToMaster = true;
		}

		public override void OnJoinedRoom()
		{
			JoinedRoom = true;
			Debug.Log("Joined random room.");
		}

		public override void OnJoinRandomFailed(short returnCode, string message)
		{
			Debug.LogWarning("Join random room failed.");
		}

		public override void OnLeftRoom()
		{
			JoinedRoom = false;
			Debug.Log("Left room.");
		}
	}
}
using System;
using System.Collections;
using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public abstract class UIPanelBase : MonoBehaviour
	{
		private const string _animOpen = "Open";
		private const string _animClose = "Close";

		[SerializeField]
		private Animator _animator = default;
		[SerializeField]
		private float _openDuration = 0.4f;
		[SerializeField]
		private float _closeDuration = 0.4f;

		private Coroutine _process;

		public abstract UIState Type { get; }

		public bool IsProcessing => _process != null;

		public bool IsOpened => gameObject.activeSelf;

		protected PlayerInfo LocalPlayerInfo => GameManager.Instance.LocalPlayerInfo;

		protected Controller LocalPlayer => GameManager.Instance.LocalPlayer;

		public void Switch()
		{
			if (gameObject.activeSelf)
			{
				Close();
			}
			else
			{
				Open();
			}
		}

		protected virtual void Initialize()
		{
		}

		protected virtual void Dispose()
		{
		}

		public void Open()
		{
			if (!IsProcessing)
			{
				StartOpen();
				_animator.Play(_animOpen, 0, 0f);
				_process = Global.Instance.StartCoroutine(DelayDo(_openDuration, OnOpened));
			}
		}

		public void Close()
		{
			if (!IsProcessing && gameObject.activeSelf)
			{
				StartClose();
				_animator.Play(_animClose, 0, 0f);
				_process = Global.Instance.StartCoroutine(DelayDo(_closeDuration, OnClosed));
			}
		}

		public void OpenImmediately()
		{
			Abort();
			StartOpen();
		}

		public void CloseImmediately()
		{
			Abort();
			OnCloseStart();
			OnClosed();
		}

		protected void Abort()
		{
			if (_process != null)
			{
				Global.Instance.StopCoroutine(_process);
				_process = null;
			}
		}

		protected virtual void OnOpenStart()
		{
		}

		protected virtual void OnOpenEnd()
		{
		}

		protected virtual void OnCloseStart()
		{
		}

		protected virtual void OnCloseEnd()
		{
		}

		private void Awake()
		{
			Initialize();
		}

		private void OnDisable()
		{
			Abort();
		}

		private void OnDestroy()
		{
			Dispose();
			Abort();
		}

		private IEnumerator DelayDo(float duration, Action onFinished)
		{
			yield return new WaitForSeconds(duration);
			onFinished?.Invoke();
		}

		private void StartOpen()
		{
			gameObject.SetActive(true);
			OnOpenStart();
		}

		private void StartClose()
		{
			UIHelper.CursorState = false;
			OnCloseStart();
		}

		private void OnOpened()
		{
			OnOpenEnd();
			UIHelper.CursorState = true;
			Abort();
		}

		private void OnClosed()
		{
			OnCloseEnd();
			gameObject.SetActive(false);
			Abort();
		}
	}
}

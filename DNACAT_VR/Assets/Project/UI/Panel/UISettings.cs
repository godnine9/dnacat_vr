using UnityEngine;
using UnityEngine.UI;

namespace Ninth.GamePlay.DNACAT
{
	public class UISettings : UIPanelBase
	{
		[SerializeField]
		private Slider _musicVolume = default;
		[SerializeField]
		private Slider _soundVolume = default;
		[SerializeField]
		private Button _buttonClose = default;
		[SerializeField]
		private Button _buttonExitRoom = default;

		public override UIState Type => UIState.Setting;

		private void Awake()
		{
			_musicVolume.value = AudioManager.GetMusicsVolume();
			_soundVolume.value = AudioManager.GetSoundsVolume();

			_musicVolume.onValueChanged.AddListener(AudioManager.SetAllMusicVolume);
			_soundVolume.onValueChanged.AddListener(AudioManager.SetAllSoundsVolume);

			_buttonClose.onClick.AddListener(Switch);
			_buttonExitRoom.onClick.AddListener(Global.Instance.GameFlow.Finish);
		}

		private void OnDestroy()
		{
			_musicVolume.onValueChanged.RemoveListener(AudioManager.SetAllMusicVolume);
			_soundVolume.onValueChanged.RemoveListener(AudioManager.SetAllSoundsVolume);

			_buttonClose.onClick.RemoveListener(Switch);

			if (Global.Instance && Global.Instance.GameFlow != null)
			{
				_buttonExitRoom.onClick.RemoveListener(Global.Instance.GameFlow.Finish);
			}
		}
	}
}

using System;
using UnityEngine;
using UnityEngine.UI;

namespace Ninth.GamePlay.DNACAT
{
	public class UIBag : UIPanelBase
	{
		[SerializeField]
		private Text _titleText = default;
		[SerializeField]
		private Text _coinText = default;
		[SerializeField]
		private Text _cookieText = default;
		[SerializeField]
		private Image _cookieIcon = default;
		[SerializeField]
		private Button _buttonClose = default;
		[SerializeField]
		private Button _buttonSell = default;
		[SerializeField]
		private Button _buttonCnacel = default;
		[SerializeField]
		private GameObject _sellOption = default;

		public override UIState Type => UIState.Bag;

		private void Awake()
		{
			_buttonSell.onClick.AddListener(SellItem);
			_buttonCnacel.onClick.AddListener(OnClickCancelSell);
			_buttonClose.onClick.AddListener(Switch);

			if (LocalPlayerInfo != null)
			{
				LocalPlayerInfo.OnUpdateItemCount += OnUpdateItemCount;
			}

			// Update all item info
			foreach (int itemID in Enum.GetValues(typeof(ItemType)))
			{
				ItemType type = (ItemType)itemID;
				OnUpdateItemCount(type, LocalPlayerInfo.Items[type].Count.Value);
			}
		}

		private void OnDestroy()
		{
			_buttonSell.onClick.RemoveListener(SellItem);
			_buttonCnacel.onClick.RemoveListener(OnClickCancelSell);
			_buttonClose.onClick.RemoveListener(Switch);

			if (LocalPlayerInfo != null)
			{
				LocalPlayerInfo.OnUpdateItemCount -= OnUpdateItemCount;
			}
		}

		protected override void OnCloseStart()
		{
			_sellOption.SetActive(false);
		}

		public void SwitchSellOption(bool active)
		{
			active &= LocalPlayer.TriggerFlags.HasFlag(TriggerFlags.Transaction);
			_sellOption.SetActive(active);
		}

		private void SellItem()
		{
			// TODO:UI operation
			var type = ItemType.Item;
			int count = 1;
			if (LocalPlayerInfo.SellItem(type, count))
			{
				int itemCount = LocalPlayerInfo.Items[type].Count.Value;
				OnUpdateItemCount(type, itemCount);

				int coinCount = LocalPlayerInfo.Items[ItemType.Coin].Count.Value;
				OnUpdateItemCount(ItemType.Coin, coinCount);
			}
		}

		private void OnClickCancelSell()
		{
			SwitchSellOption(false);
		}

		private void OnUpdateItemCount(ItemType type, int count) => UpdateItem(type, count);

		private void UpdateItem(ItemType type, int count)
		{
			switch (type)
			{
				case ItemType.Coin:
					_coinText.text = count.ToString();
					break;
				case ItemType.Item:
					_cookieText.text = count.ToString();
					break;
			}
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	public class UILobby : MonoBehaviour
	{
		[SerializeField]
		private InputField _fieldUserName;

		Preferences Preferences => Preferences.Instance;

		public string UserName
		{
			get => Preferences.UserName.Value;
			set => Preferences.UserName.Value = value;
		}

		public void OnClickStart()
		{
			if (!string.IsNullOrEmpty(_fieldUserName.text))
			{
				PhotonNetwork.LocalPlayer.NickName = _fieldUserName.text;
				Debug.Log(PhotonNetwork.LocalPlayer.NickName);
				Global.Instance.GameFlow.Finish();
			}
		}

		private void Awake()
		{
			UserName = string.Format("Player{0}", Random.Range(0, 100));
			_fieldUserName.text = UserName;
			_fieldUserName.onValueChanged.AddListener(OnUserNameChanged);
		}

		private void OnDestroy()
		{
			_fieldUserName.onValueChanged.RemoveListener(OnUserNameChanged);
		}

		private void OnUserNameChanged(string changedValue)
		{
			UserName = changedValue;
		}
	}
}
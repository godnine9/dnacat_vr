﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Chat;
using Photon.Chat.Demo;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	public class UIChat : MonoBehaviour, IChatClientListener
	{
		protected internal ChatAppSettings _chatAppSettings;

		[SerializeField]
		private GameObject _connectingLabel;
		[SerializeField]
		private Text _stateText;
		[SerializeField]
		private Text _userIdText;
		[SerializeField]
		private Text _currentChannelText;
		[SerializeField]
		private InputField _inputFieldChat;
		[SerializeField]
		private int _testLength = 2048;
		
		private ChatClient _chatClient;
		private string _selectedChannelName;
		private byte[] _testBytes = new byte[2048];

		private bool CursorState
		{
			get => UIHelper.CursorState;
			set => UIHelper.CursorState = value;
		}

		public void Connect()
		{
#if PHOTON_UNITY_NETWORKING
			_chatAppSettings = PhotonNetwork.PhotonServerSettings.AppSettings.GetChatSettings();
#endif
			_chatClient = new ChatClient(this)
			{
#if !UNITY_WEBGL
				UseBackgroundWorkerForSending = true,
#endif
				AuthValues = new AuthenticationValues(PhotonNetwork.NickName),
			};
			_chatClient.ConnectUsingSettings(_chatAppSettings);
			_connectingLabel.SetActive(true);
		}

		public void Disconnect()
		{
			_chatClient?.Disconnect();
		}

		public void SendChatMessage()
		{
			string inputLine = _inputFieldChat.text;
			if (string.IsNullOrEmpty(inputLine))
			{
				return;
			}
			if ("test".Equals(inputLine))
			{
				if (_testLength != _testBytes.Length)
				{
					_testBytes = new byte[_testLength];
				}

				_chatClient.SendPrivateMessage(_chatClient.AuthValues.UserId, _testBytes, true);
			}

			GameManager.Instance.LocalPlayer.SendShowChatBubble(inputLine);
			bool doingPrivateChat = _chatClient.PrivateChannels.ContainsKey(_selectedChannelName);
			string privateChatTarget = string.Empty;
			if (doingPrivateChat)
			{
				// the channel name for a private conversation is (on the client!!) always composed of both user's IDs: "this:remote"
				// so the remote ID is simple to figure out

				string[] splitNames = _selectedChannelName.Split(new char[] { ':' });
				privateChatTarget = splitNames[1];
			}
			//UnityEngine.Debug.Log("selectedChannelName: " + selectedChannelName + " doingPrivateChat: " + doingPrivateChat + " privateChatTarget: " + privateChatTarget);

			if (inputLine[0].Equals('\\'))
			{
				string[] tokens = inputLine.Split(new char[] { ' ' }, 2);
				if (tokens[0].Equals("\\state"))
				{
					int newState = 0;

					var messages = new List<string>
					{
						"i am state " + newState
					};
					string[] subtokens = tokens[1].Split(new char[] { ' ', ',' });

					if (subtokens.Length > 0)
					{
						newState = int.Parse(subtokens[0]);
					}

					if (subtokens.Length > 1)
					{
						messages.Add(subtokens[1]);
					}

					_chatClient.SetOnlineStatus(newState, messages.ToArray()); // this is how you set your own state and (any) message
				}
				else if ((tokens[0].Equals("\\subscribe") || tokens[0].Equals("\\s")) && !string.IsNullOrEmpty(tokens[1]))
				{
					_chatClient.Subscribe(tokens[1].Split(new char[] { ' ', ',' }));
				}
				else if ((tokens[0].Equals("\\unsubscribe") || tokens[0].Equals("\\u")) && !string.IsNullOrEmpty(tokens[1]))
				{
					_chatClient.Unsubscribe(tokens[1].Split(new char[] { ' ', ',' }));
				}
				else if (tokens[0].Equals("\\clear"))
				{
					if (doingPrivateChat)
					{
						_chatClient.PrivateChannels.Remove(_selectedChannelName);
					}
					else
					{
						if (_chatClient.TryGetChannel(_selectedChannelName, doingPrivateChat, out var channel))
						{
							channel.ClearMessages();
						}
					}
				}
				else if (tokens[0].Equals("\\msg") && !string.IsNullOrEmpty(tokens[1]))
				{
					string[] subtokens = tokens[1].Split(new char[] { ' ', ',' }, 2);
					if (subtokens.Length < 2) return;

					string targetUser = subtokens[0];
					string message = subtokens[1];
					_chatClient.SendPrivateMessage(targetUser, message);
				}
				else if ((tokens[0].Equals("\\join") || tokens[0].Equals("\\j")) && !string.IsNullOrEmpty(tokens[1]))
				{
					string[] subtokens = tokens[1].Split(new char[] { ' ', ',' }, 2);
					_chatClient.Subscribe(new string[] { subtokens[0] });
				}
				else
				{
					Debug.Log("The command '" + tokens[0] + "' is invalid.");
				}
			}
			else
			{
				if (doingPrivateChat)
				{
					_chatClient.SendPrivateMessage(privateChatTarget, inputLine);
				}
				else
				{
					_chatClient.PublishMessage(_selectedChannelName, inputLine);
				}
			}

			_inputFieldChat.text = string.Empty;
			_inputFieldChat.ActivateInputField();
		}

		public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
		{
			if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
			{
				Debug.LogError(message);
			}
			else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
			{
				Debug.LogWarning(message);
			}
			else
			{
				Debug.Log(message);
			}
		}

		public void OnConnected()
		{
			var channels = new string[] { "General" };
			_chatClient.Subscribe(channels, 10);
			_connectingLabel.SetActive(false);
			_userIdText.text = $"Connected as {PhotonNetwork.NickName}";
			_chatClient.SetOnlineStatus(ChatUserStatus.Online); // You can set your online state (without a mesage).
		}

		public void OnDisconnected()
		{
			_connectingLabel.SetActive(false);
		}

		public void OnChatStateChange(ChatState state)
		{
			_stateText.text = state.ToString();
		}

		public void OnGetMessages(string channelName, string[] senders, object[] messages)
		{
			if (channelName.Equals(_selectedChannelName))
			{
				// update text
				ShowChannel(_selectedChannelName);
			}
		}

		/// <summary>
		/// New status of another user (you get updates for users set in your friends list).
		/// </summary>
		/// <param name="user">Name of the user.</param>
		/// <param name="status">New status of that user.</param>
		/// <param name="gotMessage">True if the status contains a message you should cache locally. False: This status update does not include a
		/// message (keep any you have).</param>
		/// <param name="message">Message that user set.</param>
		public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
		{
			Debug.LogWarning("status: " + string.Format("{0} is {1}. Msg:{2}", user, status, message));
		}

		public void OnSubscribed(string[] channels, bool[] results)
		{
			foreach (string channel in channels)
			{
				_chatClient.PublishMessage(channel, "says 'hi'."); // you don't HAVE to send a msg on join but you could.
			}

			Debug.Log("OnSubscribed: " + string.Join(", ", channels));
			ShowChannel(channels[0]);
		}

		public void OnUnsubscribed(string[] channels)
		{
		}

		public void OnUserSubscribed(string channel, string user)
		{
			Debug.LogFormat("OnUserSubscribed: channel=\"{0}\" userId=\"{1}\"", channel, user);
		}

		public void OnUserUnsubscribed(string channel, string user)
		{
			Debug.LogFormat("OnUserUnsubscribed: channel=\"{0}\" userId=\"{1}\"", channel, user);
		}

		public void OnPrivateMessage(string sender, object message, string channelName)
		{
			// as the ChatClient is buffering the messages for you, this GUI doesn't need to do anything here
			// you also get messages that you sent yourself. in that case, the channelName is determinded by the target of your msg
		}

		private void Update()
		{
			_chatClient?.Service();
			HandleChatTyping();
		}

		private void OnDestroy()
		{
			Disconnect();
		}

		private void OnApplicationQuit()
		{
			Disconnect();
		}

		private void HandleChatTyping()
		{
			if (Input.GetKeyUp(KeyCode.Return) ||
				Input.GetKeyUp(KeyCode.KeypadEnter))
			{
				if (!CursorState)
				{
					_inputFieldChat.ActivateInputField();
					CursorState = true;
				}
				else if (!string.IsNullOrEmpty(_inputFieldChat.text))
				{
					SendChatMessage();
				}
				else
				{
					_inputFieldChat.DeactivateInputField();
					CursorState = false;
				}
			}
		}

		private void ShowChannel(string channelName)
		{
			if (string.IsNullOrEmpty(channelName))
			{
				return;
			}

			if (!_chatClient.TryGetChannel(channelName, out var channel))
			{
				Debug.Log("ShowChannel failed to find channel: " + channelName);
				return;
			}

			_selectedChannelName = channelName;
			_currentChannelText.text = channel.ToStringMessages();
			Debug.Log("ShowChannel: " + _selectedChannelName);
		}
	}
}
﻿using UnityEngine;

namespace Ninth.GamePlay.DNACAT
{
	public static class UIHelper
	{
		public static bool CursorState
		{
			get => Cursor.lockState == CursorLockMode.None;
			set => Cursor.lockState = value ? CursorLockMode.None : CursorLockMode.Locked;
		}

		public static void InverseCursorState()
		{
			CursorState = !CursorState;
		}
	}

	public enum CurosrState
	{
		Gameplay,
		UserInterface,
	}
}

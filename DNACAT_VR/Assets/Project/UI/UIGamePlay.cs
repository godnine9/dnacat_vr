﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;

namespace Ninth.GamePlay.DNACAT
{
	public class UIGamePlay : MonoBehaviour
	{
		[SerializeField]
		private UIBag _uiBag = default;
		[SerializeField]
		private UISettings _uiSettings = default;
		[SerializeField]
		private UIChat _chat = default;
		[SerializeField]
		private UIPlayerInfo _playerInfo = default;

		private Dictionary<UIState, UIPanelBase> _uiTable;

		public UIState CurrentState => Current != null ? Current.Type : UIState.None;

		public UIPanelBase Current
		{
			get;
			private set;
		}

		private IInputHandler InputHandler => InputManager.Instance.Handler;

		private GameManager GameManager => GameManager.Instance;

		public Controller LocalPlayer => GameManager.LocalPlayer;

		private PlayerInfo LocalPlayerInfo => GameManager.LocalPlayerInfo;

		public void SetHP(float current, float max) => _playerInfo.SetHP(current, max);

		public void SetMP(float current, float max) => _playerInfo.SetMP(current, max);

		public void ConnectChat() => _chat.Connect();

		protected void Awake()
		{
			_uiTable = new Dictionary<UIState, UIPanelBase>
			{
				{ UIState.Bag, _uiBag },
				{ UIState.Setting, _uiSettings },
			};

			if (GameManager.Instance.LocalPlayer.IsMine)
			{
				LocalPlayerInfo.OnUpdateEXP += UpdateEXP;
				LocalPlayerInfo.OnUpdateHP += SetHP;
				LocalPlayerInfo.OnUpdateMP += SetMP;

				UpdateEXP(LocalPlayerInfo.CurrentEXP, LocalPlayerInfo.MaxEXP, LocalPlayerInfo.EXP.Value);
				SetHP(LocalPlayerInfo.HP.Value, LocalPlayerInfo.MaxHP);
				SetMP(LocalPlayerInfo.MP.Value, LocalPlayerInfo.MaxMP);
				
				_playerInfo.SetName(PhotonNetwork.NickName);
				ConnectChat();
			}
		}

		private void Update()
		{
			if (InputHandler.OpenTransactionUI())
			{
				var flags = LocalPlayer.TriggerFlags;
				if (flags.HasFlag(TriggerFlags.Transaction))
				{
					SwitchUI(UIState.Bag);
				}
			}

			if (InputHandler.OpenBagUI())
			{
				SwitchUI(UIState.Bag);
			}

			if (InputHandler.OnEscape())
			{
				SwitchUI(UIState.Setting);
			}
		}

		private void OnDestroy()
		{
			_uiTable.Clear();
			_uiTable = null;
		}

		private void SwitchUI(UIState state)
		{
			foreach (var ui in _uiTable.Values)
			{
				if (ui.Type != state)
				{
					if (ui.IsOpened)
					{
						ui.Close();
					}
				}
				else
				{
					ui.Switch();
					Current = ui;
				}
			}
		}

		private void UpdateEXP(int current, int max, int currentTotal)
		{
			_playerInfo.SetEXP(current, max);
			UpdataLevel(currentTotal);
		}

		private void UpdataLevel(int currentTotal)
		{
			var levelData = PlayerLevelPreset.OrderByEXP.FirstOrDefault(element => currentTotal >= element.EXP);
			if (levelData != null)
			{
				_playerInfo.SetLevel(levelData.Level);
			}
		}
	}

	public enum UIState
	{
		None,
		Bag,
		Setting,
		Transaction,
	}
}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Ninth.GamePlay.DNACAT
{
	public class UIPlayerInfo : MonoBehaviour
	{
		[SerializeField]
		private Text _levelText = default;
		[SerializeField]
		private Text _nickNameText = default;
		[SerializeField]
		private Slider _hpSlider = default;
		[SerializeField]
		private Text _hpText = default;
		[SerializeField]
		private Slider _mpSlider = default;
		[SerializeField]
		private Text _mpText = default;
		[SerializeField]
		private Slider _expSlider = default;
		[SerializeField]
		private Text _expText = default;

		public void SetName(string name)
		{
			_nickNameText.text = name;
		}

		public void SetLevel(int level)
		{
			_levelText.text = $"Lv.{level}";
		}

		public void SetHP(float current, float max)
		{
			_hpSlider.value = current / max;
			_hpText.text = $"{current}/{max}";
		}

		public void SetMP(float current, float max)
		{
			_mpSlider.value = current / max;
			_mpText.text = $"{current}/{max}";
		}

		public void SetEXP(int current, int max)
		{
			_expSlider.value = current / (float)max;
			_expText.text = $"{current}/{max}";
		}
	}
}
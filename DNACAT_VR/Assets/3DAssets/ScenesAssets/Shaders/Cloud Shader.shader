// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/CloudShader"
{
	Properties
	{
		_BaseColor("BaseColor", Color) = (1,1,1,1)
		_NoiseTexture("NoiseTexture", 2D) = "white" {}
		_TopColor2("TopColor", Color) = (1,1,1,1)
		[Normal]_Normal("Normal", 2D) = "bump" {}
		_BotomColor2("BotomColor", Color) = (1,1,1,1)
		_Tessellation("Tessellation", Range( 0 , 10)) = 1
		_BotomLine2("BotomLine", Range( 0 , 1)) = 0.4269226
		_TopLine2("TopLine", Range( 0 , 1)) = 0.4117647
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "Tessellation.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _Normal;
		uniform sampler2D _NoiseTexture;
		uniform float4 _NoiseTexture_ST;
		uniform float4 _Normal_ST;
		uniform float4 _BaseColor;
		uniform float _TopLine2;
		uniform float4 _TopColor2;
		uniform float _BotomLine2;
		uniform float4 _BotomColor2;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _Tessellation;

		float4 tessFunction( appdata_full v0, appdata_full v1, appdata_full v2 )
		{
			float4 temp_cast_1 = (_Tessellation).xxxx;
			return temp_cast_1;
		}

		void vertexDataFunc( inout appdata_full v )
		{
			float2 uv0_NoiseTexture = v.texcoord.xy * _NoiseTexture_ST.xy + _NoiseTexture_ST.zw;
			float2 panner17 = ( 0.1 * _Time.y * float2( 1,1 ) + uv0_NoiseTexture);
			float2 panner20 = ( 0.05 * _Time.y * float2( -1,-1 ) + uv0_NoiseTexture);
			float4 temp_output_22_0 = ( tex2Dlod( _Normal, float4( panner17, 0, 0.0) ) + tex2Dlod( _Normal, float4( panner20, 0, 0.0) ) );
			v.vertex.xyz += temp_output_22_0.rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackNormal( tex2D( _Normal, uv_Normal ) );
			o.Albedo = _BaseColor.rgb;
			float temp_output_26_0 = ( i.uv_texcoord.y * 0.97 );
			float smoothstepResult33 = smoothstep( _TopLine2 , 1.0 , temp_output_26_0);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float fresnelNdotV28 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode28 = ( 0.0 + 1.0 * pow( 1.0 - fresnelNdotV28, 0.54 ) );
			float smoothstepResult36 = smoothstep( _BotomLine2 , 1.0 , ( 1.0 - temp_output_26_0 ));
			float4 Emission40 = ( ( smoothstepResult33 * ( _TopColor2 * fresnelNode28 ) ) + ( smoothstepResult36 * ( _BotomColor2 * fresnelNode28 ) ) );
			o.Emission = Emission40.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			float2 uv0_NoiseTexture = i.uv_texcoord * _NoiseTexture_ST.xy + _NoiseTexture_ST.zw;
			float2 panner5 = ( 0.1 * _Time.y * float2( 1,1 ) + uv0_NoiseTexture);
			float2 panner9 = ( 0.05 * _Time.y * float2( -1,-1 ) + uv0_NoiseTexture);
			float fresnelNdotV1 = dot( ase_worldNormal, ase_worldViewDir );
			float fresnelNode1 = ( 0.0 + 1.23 * pow( 1.0 - fresnelNdotV1, 1.0 ) );
			float4 clampResult13 = clamp( ( ( (float4( 0,0,0,0 ) + (tex2D( _NoiseTexture, panner5 ) - float4( 0.09433961,0.09433961,0.09433961,0 )) * (float4( 1,1,1,0 ) - float4( 0,0,0,0 )) / (float4( 0.9056604,0.9056604,0.9056604,0 ) - float4( 0.09433961,0.09433961,0.09433961,0 ))) * (float4( 0,0,0,0 ) + (tex2D( _NoiseTexture, panner9 ) - float4( 0.09433961,0.09433961,0.09433961,0 )) * (float4( 1,1,1,0 ) - float4( 0,0,0,0 )) / (float4( 0.9056604,0.9056604,0.9056604,0 ) - float4( 0.09433961,0.09433961,0.09433961,0 ))) ) + ( 1.0 - fresnelNode1 ) ) , float4( 0,0,0,0 ) , float4( 1,1,1,0 ) );
			o.Alpha = clampResult13.r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows vertex:vertexDataFunc tessellate:tessFunction 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17800
2214;162;1320;774;573.6084;567.8096;1.221682;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;25;-2161.038,-2174.778;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;6;-2015.022,-251.8798;Inherit;False;0;3;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;8;-2026.461,-24.02414;Inherit;False;0;3;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1932.667,-2250.009;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0.97;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;31;-1419.624,-2305.696;Inherit;False;Property;_TopColor2;TopColor;2;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-1592.266,-1516.27;Inherit;False;Property;_BotomLine2;BotomLine;6;0;Create;True;0;0;False;0;0.4269226;0.703;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;32;-1675.623,-2012.136;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;30;-1446.519,-1361.573;Inherit;False;Property;_BotomColor2;BotomColor;4;0;Create;True;0;0;False;0;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FresnelNode;28;-1886.459,-1697.952;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0.54;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-1620.113,-2434.704;Inherit;False;Property;_TopLine2;TopLine;7;0;Create;True;0;0;False;0;0.4117647;0.4117647;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;9;-1767.94,-19.50875;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;0.05;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;5;-1756.501,-247.3644;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;0.1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;10;-1467.652,-34.18414;Inherit;True;Property;_TextureSample0;Texture Sample 0;1;0;Create;True;0;0;False;0;-1;fff70cc323e7244f0a6fed5ecb11a9a1;fff70cc323e7244f0a6fed5ecb11a9a1;True;0;False;white;Auto;False;Instance;3;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;3;-1456.212,-262.0398;Inherit;True;Property;_NoiseTexture;NoiseTexture;1;0;Create;True;0;0;False;0;-1;fff70cc323e7244f0a6fed5ecb11a9a1;09cbe3235c651824dad46f730cfdf9d8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-1171.79,-2314.91;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;33;-1308.522,-2566.133;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-1165.808,-1534.615;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;36;-1390.488,-1814.24;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;1;-1620.548,238.7078;Inherit;True;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1.23;False;3;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;19;-964.2405,808.3776;Inherit;False;0;3;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;16;-955.5282,552.3568;Inherit;False;0;3;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-968.4476,-2520.727;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;38;-1032.576,-1715.065;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;46;-1103.221,-280.7302;Inherit;True;5;0;COLOR;0,0,0,0;False;1;COLOR;0.09433961,0.09433961,0.09433961,0;False;2;COLOR;0.9056604,0.9056604,0.9056604,0;False;3;COLOR;0,0,0,0;False;4;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;44;-1113.621,-44.13017;Inherit;True;5;0;COLOR;0,0,0,0;False;1;COLOR;0.09433961,0.09433961,0.09433961,0;False;2;COLOR;0.9056604,0.9056604,0.9056604,0;False;3;COLOR;0,0,0,0;False;4;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;39;-756.7745,-2183.608;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PannerNode;17;-697.0072,556.8722;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;0.1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-834.1212,-171.5302;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PannerNode;20;-705.7195,811.2332;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-1,-1;False;1;FLOAT;0.05;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;2;-1270.809,246.4668;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;40;-384.3086,-2119.452;Inherit;False;Emission;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;11;-474.0849,30.14789;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;21;-405.4308,798.2175;Inherit;True;Property;_TextureSample2;Texture Sample 2;3;0;Create;True;0;0;False;0;-1;fff70cc323e7244f0a6fed5ecb11a9a1;fff70cc323e7244f0a6fed5ecb11a9a1;True;0;False;white;Auto;False;Instance;15;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;18;-396.7185,542.1968;Inherit;True;Property;_TextureSample1;Texture Sample 1;3;0;Create;True;0;0;False;0;-1;fff70cc323e7244f0a6fed5ecb11a9a1;fff70cc323e7244f0a6fed5ecb11a9a1;True;0;False;white;Auto;False;Instance;15;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;41;63.35864,-113.3755;Inherit;False;40;Emission;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;24;11.07445,797.8918;Inherit;False;Property;_Tessellation;Tessellation;5;0;Create;True;0;0;False;0;1;2.21;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;47;122.5327,-4.817688;Inherit;False;Property;_Metallic;Metallic;8;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;139.5327,81.18231;Inherit;False;Property;_Smoothness;Smoothness;9;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;22;-75.19047,506.9162;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;14;-129.8793,-678.7207;Inherit;False;Property;_BaseColor;BaseColor;0;0;Create;True;0;0;False;0;1,1,1,1;0.8207547,0.8207547,0.8207547,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;23;133.5169,497.1264;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;15;-239.5258,-430.3196;Inherit;True;Property;_Normal;Normal;3;1;[Normal];Create;True;0;0;False;0;-1;f74cd243c13f81f419a1142891b0e1ed;f74cd243c13f81f419a1142891b0e1ed;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ClampOpNode;13;-189.8279,12.87246;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,1,1,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;355.4353,-55.10626;Float;False;True;-1;6;ASEMaterialInspector;0;0;Standard;Custom/CloudShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;True;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;26;0;25;2
WireConnection;32;0;26;0
WireConnection;9;0;8;0
WireConnection;5;0;6;0
WireConnection;10;1;9;0
WireConnection;3;1;5;0
WireConnection;34;0;31;0
WireConnection;34;1;28;0
WireConnection;33;0;26;0
WireConnection;33;1;29;0
WireConnection;35;0;30;0
WireConnection;35;1;28;0
WireConnection;36;0;32;0
WireConnection;36;1;27;0
WireConnection;37;0;33;0
WireConnection;37;1;34;0
WireConnection;38;0;36;0
WireConnection;38;1;35;0
WireConnection;46;0;3;0
WireConnection;44;0;10;0
WireConnection;39;0;37;0
WireConnection;39;1;38;0
WireConnection;17;0;16;0
WireConnection;45;0;46;0
WireConnection;45;1;44;0
WireConnection;20;0;19;0
WireConnection;2;0;1;0
WireConnection;40;0;39;0
WireConnection;11;0;45;0
WireConnection;11;1;2;0
WireConnection;21;1;20;0
WireConnection;18;1;17;0
WireConnection;22;0;18;0
WireConnection;22;1;21;0
WireConnection;23;0;22;0
WireConnection;13;0;11;0
WireConnection;0;0;14;0
WireConnection;0;1;15;0
WireConnection;0;2;41;0
WireConnection;0;3;47;0
WireConnection;0;4;48;0
WireConnection;0;9;13;0
WireConnection;0;11;22;0
WireConnection;0;14;24;0
ASEEND*/
//CHKSM=0ACE4E38875B113A23256A19632E13D109EAB5B8